package bran.cma;

public class Pop extends Instruction {
    
    public void exec(CMA state) {
	state.SP--;
    }

}
