package bran.compiler;

import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bran.data.structures.DataType;
import bran.data.structures.Definition;
import bran.data.structures.StructDef;
import bran.data.structures.SymbolTable;
import bran.data.structures.tree.ControlNodeFunction;
import bran.data.structures.tree.VarsNode;
import bran.data.structures.tree.VarsNodeBoolean;
import bran.data.structures.tree.VarsNodeInt;
import bran.data.structures.tree.VarsNodeStruct;
import bran.exceptions.InvalidTreeStructureException;
import bran.exceptions.RootException;
import bran.parser.BRAN_AST_AddSubOptional;
import bran.parser.BRAN_AST_AllocOP;
import bran.parser.BRAN_AST_ArrayCall;
import bran.parser.BRAN_AST_ArrayDeclaration;
import bran.parser.BRAN_AST_Block;
import bran.parser.BRAN_AST_ComparisonOptional;
import bran.parser.BRAN_AST_ConditionalExpression;
import bran.parser.BRAN_AST_EqualsOptional;
import bran.parser.BRAN_AST_ExpressionValues;
import bran.parser.BRAN_AST_FullAssignment;
import bran.parser.BRAN_AST_Function;
import bran.parser.BRAN_AST_FunctionCall;
import bran.parser.BRAN_AST_FunctionDeclaration;
import bran.parser.BRAN_AST_Identifier;
import bran.parser.BRAN_AST_IterationStatement;
import bran.parser.BRAN_AST_LVStructureOptional;
import bran.parser.BRAN_AST_LogicalAndOptional;
import bran.parser.BRAN_AST_LogicalOrOptional;
import bran.parser.BRAN_AST_LowAssignOrIncrement;
import bran.parser.BRAN_AST_MultDivModOptional;
import bran.parser.BRAN_AST_Parameter;
import bran.parser.BRAN_AST_ParameterDeclaration;
import bran.parser.BRAN_AST_ParameterList;
import bran.parser.BRAN_AST_Pointer;
import bran.parser.BRAN_AST_ReturnStatement;
import bran.parser.BRAN_AST_SelectionStatement;
import bran.parser.BRAN_AST_Sequence;
import bran.parser.BRAN_AST_TypeSpecifier;
import bran.parser.BRAN_AST_UnaryOP;
import bran.parser.BRAN_AST_VariableDeclaration;
import bran.parser.Node;
import bran.parser.SimpleNode;
import bran.parser.Token;

public class FunctionHandler implements Runnable
{
	private static final Logger					log						= LogManager.getLogger();
	private BRAN_AST_Block						block;
	private CodeGenTreeBuilder					codegenbuild;
	private final ArrayList<BRAN_AST_Sequence>	codegenhandledsequences	= new ArrayList<>();
	private final ArrayList<VarsNode>			codegenparams			= new ArrayList<>();
	private ControlNodeFunction					codegenroot;
	private final ArrayList<SimpleNode>			handledIdentifiers		= new ArrayList<>();
	private boolean								isReturned				= false;
	private ArrayList<CountDownLatch>			latches;
	private final ArrayList<Definition>			params					= new ArrayList<>();
	/**
	 * Currently the stack handles only string values but this is bullshit. Possible we need a new
	 * class?
	 */
	private final Stack<String>					pda						= new Stack<>();
	private boolean								processed				= false;
	private final BRAN_AST_Function				root;

	private Definition							self;

	private final ArrayList<BRAN_AST_Sequence>	sequences				= new ArrayList<>();
	private final SymbolTable					symbols					= new SymbolTable();

	private final AtomicInteger					varScopeCounterParam	= new AtomicInteger(0);
	private final AtomicInteger					varScopeCounterVar		= new AtomicInteger(0);

	/**
	 * Creates a new FunctionHandler. By instantiating this class it extracts the functions name,
	 * return type, parameter list and body (if present). If the function node is only a header
	 * definition the body can be added later.
	 *
	 * @param func
	 */
	public FunctionHandler(final BRAN_AST_Function func)
	{
		root = func;
		for (int i = 0; i < func.jjtGetNumChildren(); i++) // function children
		{
			final Node header = func.jjtGetChild(i);
			if (header instanceof BRAN_AST_FunctionDeclaration)
			{
				final SimpleNode type = (SimpleNode) header.jjtGetChild(0);
				final SimpleNode decl = (SimpleNode) header.jjtGetChild(1);
				self = new Definition(decl.jjtGetValue().toString(), type.jjtGetValue().toString(),
						((Token) decl.jjtGetValue()).beginLine);
				// self.setVarScopeCounter(varScopeCounter.incrementAndGet());
				if (decl.jjtGetNumChildren() > 0) // we got params
				{

					if (decl.jjtGetChild(0) instanceof BRAN_AST_Parameter)
					{
						handleParams((BRAN_AST_Parameter) decl.jjtGetChild(0));
					} else
					{
						for (int x = 0; x < decl.jjtGetNumChildren(); x++)
						{
							final Node paramDecl = decl.jjtGetChild(x);
							if (paramDecl instanceof BRAN_AST_ParameterDeclaration)
							{
								final Definition def = new Definition((BRAN_AST_ParameterDeclaration) paramDecl);
								def.setVarScopeCounter(varScopeCounterVar.incrementAndGet());
								params.add(def);
								switch (def.getType().toString())
								{
								case "BOOL":
									codegenparams.add(new VarsNodeBoolean((SimpleNode) paramDecl, def));
									break;
								case "INT":
									codegenparams.add(new VarsNodeInt((SimpleNode) paramDecl, def));
									break;
								case "STRUCT":
									codegenparams.add(new VarsNodeStruct((SimpleNode) paramDecl, def));
									break;
								default:
									throw new InvalidTreeStructureException(def.toString());
								}

							} else
							{
								log.error("This should not be reachable - please check grammar and AST");
							}
						}
					}
				}
			}
			if (header instanceof BRAN_AST_Block)
			{
				block = (BRAN_AST_Block) header;
			}
		}
	}

	/**
	 * Check which type the given parameter has and adds the type to the stack.
	 *
	 * @param value
	 */
	private void addExpValue(final SimpleNode value)
	{
		if (typeCheck(value.jjtGetValue().toString(), DataType.INT))
		{
			LogMessages.logPushExpressionValue(value, DataType.INT);
			pda.push("int");
		} else if (typeCheck(value.jjtGetValue().toString(), DataType.BOOL))
		{
			LogMessages.logPushExpressionValue(value, DataType.BOOL);
			pda.push("bool");
		} else if (value.jjtGetValue().toString().equals("NULL"))
		{
			LogMessages.logPushExpressionValue(value, DataType.VOID);
			pda.push("NULL");
		} else
		{
			log.error("Unknown ExprValueType:[" + value.jjtGetValue() + "].");
		}
	}

	/**
	 * If we currently only have the function header and are missing the block / body of the
	 * function, this is to add the block
	 *
	 * @param func
	 */
	public void addFuncBody(final BRAN_AST_Block func)
	{
		if (block == null)
		{
			block = func;
		} else
		{
			log.error("Block already set");
		}
	}

	/**
	 * to be called by whatever function needs to be notified when this function instance has been
	 * processed
	 *
	 * @param latch
	 */
	public void addLatch(final CountDownLatch latch)
	{
		if (latches == null)
		{
			latches = new ArrayList<>();
		}
		latches.add(latch);
	}

	private void checkProto() throws RootException
	{
		// Check if stack is empty. This is mandatory
		if (!pda.isEmpty())
		{
			pda.clear();
		}

		for (int i = 0; i < block.jjtGetNumChildren(); i++)
		{
			checkSequenceChilds(((SimpleNode) block.jjtGetChild(i)), ((SimpleNode) block.jjtGetChild(i)));
		}

		/**
		 * As last part of our type checking this method checks if a return statement is necessary
		 * and if it part of the function body.
		 */
		if (!self.getType().toString().equals("VOID"))
		{
			if (!isReturned)
			{
				LogMessages.logErrorMessageWithExcep("Function [" + getFuncName() + "] expected a return type ["
						+ getReturnType() + "] but no return was found in function body.");
			}
		}
	}

	@SuppressWarnings("unused")
	private void checkSequenceChilds(final SimpleNode seq, final SimpleNode rootSequence) throws RootException
	{

		if (seq.jjtGetNumChildren() > 0)
		{
			for (int i = 0; i < seq.jjtGetNumChildren(); i++)
			{
				checkSequenceChilds((SimpleNode) seq.jjtGetChild(i), rootSequence);
			}
		}
		// Primitive Types
		if (seq instanceof BRAN_AST_Identifier)
		{
			/*
			 * Check if the Variable is an Array or not. If it is an array then we need an index
			 * Example: A, B are int arrays int c = A[1] + B[2]; is fine while int c = A + B[2];
			 * should not work
			 */
			// @formatter:off
			if (symbols.contains(seq.jjtGetValue().toString())
					&& symbols.get(seq.jjtGetValue().toString()).isArray() /* Check if identififier is an array */
					&& !(seq.jjtGetParent() instanceof BRAN_AST_ArrayCall) /* check if its nothing like for example int a = b[5]; this will be handled seperately*/
					&& symbols.get(seq.jjtGetValue().toString()).isAllocated() /* our arrays has to be allocated */
					&& !(seq.jjtGetParent() instanceof BRAN_AST_ParameterList) /*
					 * if it is part of
					 * an parameterlist
					 * its getting
					 * checked somewhere
					 * else
					 */
					// @formatter:on
					)
			{
				LogMessages.logErrorMessageWithExcep("Var [" + ((Token) seq.jjtGetValue()).image + "] has no index at "
						+ ((Token) seq.jjtGetValue()).beginLine);
			}
			String type = null;
			if (seq.jjtGetParent() instanceof BRAN_AST_FunctionCall)
			{
				try
				{
					type = Main.getFunctionHandler(seq.jjtGetValue().toString()).getReturnType().toString()
							.toLowerCase();
//					if (seq.jjtGetParent().jjtGetParent() instanceof BRAN_AST_FullAssignment
//							|| seq.jjtGetParent().jjtGetParent() instanceof BRAN_AST_LowAssignOrIncrement) {
//						pda.push(type);
//						log.debug("Push function identifier [" + ((Token) seq.jjtGetValue()).image + ":" + type
//								+ "], at " + ((Token) seq.jjtGetValue()).beginLine + ":"
//								+ ((Token) seq.jjtGetValue()).beginColumn);
//					}
					if (!(seq.jjtGetParent().jjtGetParent() instanceof BRAN_AST_Sequence)){
						pda.push(type);
						log.debug("Push function identifier [" + ((Token) seq.jjtGetValue()).image + ":" + type
								+ "], at " + ((Token) seq.jjtGetValue()).beginLine + ":"
								+ ((Token) seq.jjtGetValue()).beginColumn);
					}
					else
					{
						//Do nothing because we have an non used function call
					}
				} catch (final Exception e)
				{
					LogMessages.logErrorMessageWithExcep(
							"Invalid function call: " + seq.jjtGetValue() + " detected at line: " + (seq.jjtGetValue())
							+ ". You have not specified a function with the identifier: " + seq.jjtGetValue());
				}
			} else if (seq.jjtGetParent() instanceof BRAN_AST_ParameterList)
			{
				try
				{
					pda.push(getVariable(seq.jjtGetValue().toString()).getType().toString().toLowerCase());
				} catch (final Exception e)
				{
					LogMessages.logUnknownIdentifierWithException(seq);
				}
			}
			// At this point struct processing is getting very ugly
			else if ((seq.jjtGetParent() instanceof BRAN_AST_TypeSpecifier)
					&& ((SimpleNode) seq.jjtGetParent()).jjtGetValue().toString().equals("struct"))
			{
				/*
				 * struct x *s At this point the detected identifier is x and we have to do nothing
				 * because we need the identifier s not the struct type x.
				 */
			} else if ((seq.jjtGetParent() instanceof BRAN_AST_VariableDeclaration)
					&& ((SimpleNode) seq.jjtGetParent().jjtGetChild(0)).jjtGetValue().toString().equals("struct"))
			{
				// Now we have our required identifier s and we have to push the
				// type
				// struct
				log.debug("Push identifier [" + ((Token) seq.jjtGetValue()).image + ":"
						+ ((SimpleNode) seq.jjtGetParent().jjtGetChild(0)).jjtGetValue().toString() + "], at "
						+ ((Token) seq.jjtGetValue()).beginLine + ":" + ((Token) seq.jjtGetValue()).beginColumn);
				pda.push(((SimpleNode) seq.jjtGetParent().jjtGetChild(0)).jjtGetValue().toString());
			} else if (seq.jjtGetParent() instanceof BRAN_AST_LVStructureOptional)
			{
				/*
				 * There are only two options: First: It must be the type of a struct Second: It
				 * must be a field of a struct Otherwise: Bullshit
				 */
				if (symbols.contains(seq.jjtGetValue().toString())
						&& symbols.get(seq.jjtGetValue().toString()).isStruct())
				{
					pda.push("struct");
				} else
				{
					// Avoids NullPointerException if identifier is not declared
					// or not from type struct
					try
					{
						final StructDef tmpStruct = Main.getStruct(
								symbols.get(((SimpleNode) seq.jjtGetParent().jjtGetChild(0)).jjtGetValue().toString())
								.getStructDef().getName());
						if (tmpStruct.contains(seq.jjtGetValue().toString()))
						{
							log.debug("Push identifier [" + ((Token) seq.jjtGetValue()).image + ":"
									+ tmpStruct.getField(seq.jjtGetValue().toString()).getType().toString()
									.toLowerCase()
									+ "], at " + ((Token) seq.jjtGetValue()).beginLine + ":"
									+ ((Token) seq.jjtGetValue()).beginColumn);
							pda.push(tmpStruct.getField(seq.jjtGetValue().toString()).getType().toString()
									.toLowerCase());
						}
						// The used identifier is a local variable from the
						// function block
						else if (symbols.contains(seq.jjtGetValue().toString()))
						{
							LogMessages.logErrorMessageWithExcep("Error in STRUCT operation -> at line "
									+ ((Token) seq.jjtGetValue()).beginLine + ": " + "The used identifier "
									+ seq.jjtGetValue() + " is neither from type struct nor a field of the struct");
						}
						// The used identifier was never declared
						else
						{
							LogMessages.logUnknownIdentifierWithException(seq);
						}
					} catch (final NullPointerException e)
					{
						LogMessages.logErrorMessageWithExcep("Error in STRUCT operation -> at line "
								+ ((Token) seq.jjtGetValue()).beginLine + ": " + "The used identifier "
								+ seq.jjtGetValue() + " is neither from type struct nor a field of the struct");
					}
				}
			}
			//Filter Code like int a;
			else if ((((SimpleNode) seq.jjtGetParent().jjtGetParent()) instanceof BRAN_AST_FullAssignment)
					&&  (((SimpleNode) seq.jjtGetParent().jjtGetParent()).jjtGetValue() == null)){
				//Corresponds to a declaration like int a
				//There is no need to push this type
			}
			else
			{
				try
				{
					type = getVariable(seq.jjtGetValue().toString()).getType().toString().toLowerCase();
					log.debug("Push identifier [" + ((Token) seq.jjtGetValue()).image + ":" + type + "], at "
							+ ((Token) seq.jjtGetValue()).beginLine + ":" + ((Token) seq.jjtGetValue()).beginColumn);
					pda.push(type);
				} catch (final Exception e)
				{
					LogMessages.logUnknownIdentifierWithException(seq);
				}
			}
		} else if (seq instanceof BRAN_AST_ExpressionValues)
		{
			if (seq.jjtGetParent() instanceof BRAN_AST_AllocOP)
			{
				// Dirty little workaround i guess: if we want to do sth like
				// bool [] A
				// = alloc_array(bool, 10); it seems like he wants to check the
				// 10 against an bool value which is useless because the 10 is
				// not type but the array size
				pda.push(((SimpleNode) seq.jjtGetParent().jjtGetChild(0)).jjtGetValue().toString());

			} else if (seq.jjtGetParent() instanceof BRAN_AST_ParameterList)
			{
				addExpValue(seq);
			} else
			{
				addExpValue(seq);
			}
		}
		// Check types from left to right for equality
		else if (seq instanceof BRAN_AST_FullAssignment)
		{
			// Differentiate between Full (=) and Low (null)
			if (seq.jjtGetValue() != null)
			{
				final String first = pda.pop();
				final String second = pda.pop();
				if (!first.equals(second))
				{
					LogMessages.logErrorMessageWithExcep(
							"Type mismatch: [Value:" + ((Token) seq.jjtGetValue()).next.image + "],[Type:" + first
							+ "],[Expected:" + second + "], at " + ((Token) seq.jjtGetValue()).beginLine + ":"
							+ ((Token) seq.jjtGetValue()).beginColumn);
				} else
				{
					log.debug("Full assignment completed at " + ((Token) seq.jjtGetValue()).beginLine);
					//We need this in order to allow constructs like for(int i = 0; i < 10; i++)
					if(seq.jjtGetParent() instanceof BRAN_AST_IterationStatement)
						pda.push(first);
				}
			}
		} else if (seq instanceof BRAN_AST_LowAssignOrIncrement)
		{
			lowAssignment(seq);
		} else if (seq instanceof BRAN_AST_ReturnStatement)
		{
			/*
			 * Check if the function is void and if it has an return statement
			 */
			if (getReturnType().toString().toLowerCase().equals("void"))
			{
				LogMessages.logErrorMessageWithExcep(
						"Function: " + getFuncName() + " does not expect a return statement but was detected at line: "
								+ ((Token) seq.jjtGetValue()).beginLine);
			}
			/*
			 * Check if the type equals the type of the return statement
			 */
			else
			{
				final String stackReturn = pda.pop();
				if (getReturnType().toString().toLowerCase().equals(stackReturn))
				{
					log.debug("Return type of function: " + getFuncName() + " completed at line: "
							+ ((Token) seq.jjtGetValue()).beginLine);
					isReturned = true;
				} else
				{
					LogMessages.logErrorMessageWithExcep("Type mismatch in return type at function: " + getFuncName()
					+ " at line: " + ((Token) seq.jjtGetValue()).beginLine + ". Expected type: "
					+ getReturnType() + " but get: " + stackReturn.toUpperCase());
				}
			}
		} else if (seq instanceof BRAN_AST_FunctionCall)
		{
			/**
			 * In this stage all the types from the parameter list should be in the stack and can be
			 * checked against the original type defined in the function definition. We also need to
			 * check if the variables are for example an array/struct or not and if they are
			 * allocated
			 */
			final String funcIdentifier = ((SimpleNode) seq.jjtGetChild(0)).jjtGetValue().toString();
			final int numArgs = Main.getFunctionHandler(funcIdentifier).getParamSize();
			final ArrayList<Definition> tmpDefinition = Main.getFunctionHandler(funcIdentifier).params;
			final SimpleNode parameterList = ((SimpleNode) seq.jjtGetChild(1));
			if (numArgs == parameterList.jjtGetNumChildren())
			{
				for (int i = numArgs - 1; i >= 0; i--)
				{
					final SimpleNode identifier = (SimpleNode) parameterList.jjtGetChild(i);
					final String paramType = pda.pop();
					if (tmpDefinition.get(i).getType().toString().toLowerCase().equals(paramType))
					{
						// Expects the function itself an array? If yes then we
						// need an array in the call also
						if (tmpDefinition.get(i).isArray()
								&& (symbols.get(identifier.jjtGetValue().toString()) != null)
								&& symbols.get(identifier.jjtGetValue().toString()).isArray())
						{
							log.debug("Correct ARRAY parameter ["
									+ symbols.get(identifier.jjtGetValue().toString()).getName() + "] type "
									+ paramType.toUpperCase() + "[isArray:"
									+ symbols.get(identifier.jjtGetValue().toString()).isArray() + "][isAllocated:"
									+ symbols.get(identifier.jjtGetValue().toString()).isAllocated()
									+ "] for function: " + funcIdentifier + " at line: "
									+ ((Token) seq.jjtGetValue()).beginLine);
							continue;
						} else
						{
							if (tmpDefinition.get(i).isArray()
									|| ((symbols.get(identifier.jjtGetValue().toString()) != null)
											&& symbols.get(identifier.jjtGetValue().toString()).isArray()))
							{
								LogMessages.logErrorMessageWithExcep("Incorrect ARRAY paramter ["
										+ symbols.get(identifier.jjtGetValue().toString()).getName() + "] type ["
										+ paramType.toUpperCase() + "] [ExpectedAnArray:"
										+ tmpDefinition.get(i).isArray() + "][isArray:"
										+ symbols.get(identifier.jjtGetValue().toString()).isArray()
										+ "] for function: " + funcIdentifier + " at line: "
										+ ((Token) seq.jjtGetValue()).beginLine);
							}
						}
						log.debug("Correct parameter [" + (symbols.get(identifier.jjtGetValue().toString()) != null
								? symbols.get(identifier.jjtGetValue().toString()).getName() : paramType)
								+ "] type " + paramType.toUpperCase() + "[isArray:"
								+ (symbols.get(identifier.jjtGetValue().toString()) != null
								? symbols.get(identifier.jjtGetValue().toString()).isArray() : "false")
								+ "][isAllocated:"
								+ (symbols.get(identifier.jjtGetValue().toString()) != null
								? symbols.get(identifier.jjtGetValue().toString()).isAllocated() : "false")
								+ "] for function: " + funcIdentifier + " at line: "
								+ ((Token) seq.jjtGetValue()).beginLine);
						continue;
					} else
					{
						LogMessages.logErrorMessageWithExcep(
								"Incorrect paramter type " + paramType.toUpperCase() + " for function: "
										+ funcIdentifier + " at line: " + ((Token) seq.jjtGetValue()).beginLine);
					}

				}
			} else
			{
				LogMessages.logErrorMessageWithExcep("Function call at line: " + ((Token) seq.jjtGetValue()).beginLine
						+ " does not contain the correct number of parameters as specified in definition."
						+ " Expected: " + numArgs + " found: " + parameterList.jjtGetNumChildren());
			}
		}
		// && and ||
		else if ((seq instanceof BRAN_AST_LogicalAndOptional) || (seq instanceof BRAN_AST_LogicalOrOptional))
		{
			// Both types must be of type BOOL
			final String rightV = pda.pop(), leftV = pda.pop();
			if (leftV.equals("bool") && rightV.equals("bool"))
			{
				log.debug("Correct types at LogicalAndOption statement at line: "
						+ ((Token) seq.jjtGetValue()).beginLine);
				// Push return type BOOL for further parsing stages
				pda.push("bool");
			} else
			{
				LogMessages.logErrorMessageWithExcep("Incorrect types at line: " + ((Token) seq.jjtGetValue()).beginLine
						+ ". Logical-And-Expression expects type bool on both sites.");
			}
		}
		// == | !=
		else if (seq instanceof BRAN_AST_EqualsOptional)
		{
			/**
			 * There are multiple types for both sides. Both sides must have the same type. They do
			 * not apply to arguments of type string and struct s int, bool, t [] and t * are
			 * allowed.
			 *
			 * TODO: Handle structs which are forbidden in this case (strings are not implemented)
			 */
			if (pda.pop().equals(pda.pop()))
			{
				log.debug("Correct types at ComparisonStatement statement at line: "
						+ ((Token) seq.jjtGetValue()).beginLine);
				// Push return type BOOL for further parsing stages
				pda.push("bool");
			} else
			{
				LogMessages.logErrorMessageWithExcep("Incorrect types at line: " + ((Token) seq.jjtGetValue()).beginLine
						+ ". Comparison-Expression expects type int, bool, t [] or t* on both sites.");
			}
		}
		// < | <= | > | >=
		else if (seq instanceof BRAN_AST_ComparisonOptional)
		{
			// Both sides must be of type INT
			final String rightV = pda.pop(), leftV = pda.pop();
			if (leftV.equals("int") && rightV.equals("int"))
			{
				log.debug("Correct types at ComparisonStatement statement at line: "+((Token)seq.jjtGetValue()).beginLine);
				// Push return type BOOL for further parsing stages
				pda.push("bool");
			} else
			{
				LogMessages.logErrorMessageWithExcep("Incorrect types at line: " + ((Token) seq.jjtGetValue()).beginLine
						+ ". Comparison-Expression expects type INT on both sites.");
			}
		}
		// + | -
		else if ((seq instanceof BRAN_AST_AddSubOptional) || (seq instanceof BRAN_AST_MultDivModOptional))
		{
			// Both sides must be of type INT
			final String leftV = pda.pop(), rightV = pda.pop();
			if (leftV.equals("int") && rightV.equals("int"))
			{
				log.debug("Correct types at AddSubMultDivMod statement at line: "
						+ ((Token) seq.jjtGetValue()).beginLine);
				// Push return type INT for further parsing stages
				pda.push("int");
			} else
			{
				LogMessages.logErrorMessageWithExcep("Incorrect types at line: " + ((Token) seq.jjtGetValue()).beginLine
						+ ". AddSubMultDivMod expects type INT on both sites.");
			}
		} else if (seq instanceof BRAN_AST_AllocOP)
		{
			String id;
			if (seq.jjtGetParent() instanceof BRAN_AST_LowAssignOrIncrement)
			{
				id = ((SimpleNode) seq.jjtGetParent().jjtGetChild(0)).jjtGetValue().toString();
			} else
			{
				if ((seq.jjtGetNumChildren() > 0)
						&& ((SimpleNode) seq.jjtGetChild(0)).jjtGetValue().toString().equals("struct"))
				{
					// Get the correct identifier and not the type of struct
					final BRAN_AST_Identifier structIdent = (BRAN_AST_Identifier) seq.jjtGetParent().jjtGetChild(0)
							.jjtGetChild(2);

					id = structIdent.jjtGetValue().toString();
					pda.push("struct");
				} else
				{
					id = ((SimpleNode) seq.jjtGetParent().jjtGetChild(0).jjtGetChild(1)).jjtGetValue().toString();
					symbols.get(id).setArray(true);
				}
			}
			symbols.get(id).setAllocated(true);
		} else if (seq instanceof BRAN_AST_ArrayCall)
		{
			final String id = ((SimpleNode) seq.jjtGetChild(0)).jjtGetValue().toString();
			if (symbols.get(id).isAllocated())
			{
				log.debug("Var [" + id + "] is allocated at " + ((Token) seq.jjtGetValue()).beginLine);
			} else
			{
				LogMessages.logErrorMessageWithExcep("Var [" + id + "] is not allocated or not an array at "
						+ ((Token) seq.jjtGetValue()).beginLine + "!");
			}
			if(pda.pop().equals("int"))
			{
				log.debug("Correct ArrayCall [" + id + "] at " + ((Token) seq.jjtGetValue()).beginLine);
			}
			else
			{
				LogMessages.logErrorMessageWithExcep("Incorrect ArrayCall [" + id + "] at " + ((Token) seq.jjtGetValue()).beginLine + "!");
			}

		} else if (seq instanceof BRAN_AST_ConditionalExpression)
		{
			// Conditional Statements have 3 variables on the stack: first pop()
			// is false-part, second pop() is the true-part, third pop() is the
			// condition part
			final String cond2 = pda.pop();
			final String cond1 = pda.pop();
			final String cond = pda.pop();
			// condition has 2 be type bool
			if (cond.equals("bool"))
			{
				// both conditions have 2 be the same
				if (cond1.equals(cond2))
				{
					// Push one type of condition back to the stack in order to
					// allow further processing of that statement
					pda.push(cond1);
				} else
				{
					LogMessages.logErrorMessageWithExcep("Incorrect Types with [" + seq + "]: [TrueCondition:" + cond1
							+ "][FalseCondition:" + cond2 + "] at " + ((Token) seq.jjtGetValue()).beginLine);
				}
			} else
			{
				LogMessages.logErrorMessageWithExcep("Incorrect Types with [" + seq + "]: [Condition:" + cond
						+ "][ExpectedCondition:bool] at " + ((Token) seq.jjtGetValue()).beginLine);
			}

		} else if (seq instanceof BRAN_AST_LVStructureOptional)
		{
			if (seq.jjtGetValue().toString().equals("->"))
			{
				final String structField = pda.pop();
				pda.pop();

				// The types from struct and structField were already checked
				// against the struct
				// itself
				// The result is type of structField
				pda.push(structField);
			} else
			{
				// (*s).x ...
				// Muss vohrer gepointert werden!!! Wie pr�fe ich das?
				LogMessages.logErrorMessageWithExcep("The usage of the STRUCT operation . "
						+ "is prohibited. Error at line: " + ((Token) seq.jjtGetValue()).beginLine);
			}
		} else if (seq instanceof BRAN_AST_UnaryOP)
		{
			final String operator = seq.jjtGetValue().toString();
			final String type = pda.pop();
			switch (operator)
			{
			case "!":
				if (type.equals("bool"))
				{
					log.debug("UnaryOperator [!]: Correct Type[isType:" + type + "] [ExpectedType:bool] at "
							+ ((Token) seq.jjtGetValue()).beginLine);
				} else
				{
					LogMessages.logErrorMessageWithExcep("UnaryOperator [!]: Invalid Type[isType:" + type
							+ "] [ExpectedType:bool] at " + ((Token) seq.jjtGetValue()).beginLine);
				}
				break;
			case "-":
				if (type.equals("int"))
				{
					log.debug("UnaryOperator [-]: Correct Type[isType:" + type + "] [ExpectedType:bool] at "
							+ ((Token) seq.jjtGetValue()).beginLine);
				} else
				{
					LogMessages.logErrorMessageWithExcep("UnaryOperator [-]: Invalid Type[isType:" + type
							+ "] [ExpectedType:int] at " + ((Token) seq.jjtGetValue()).beginLine);
				}
				break;
			case "*":
				if (true)
				{
					// TODO: was genau hierbei zu machen ist wei� ich nicht so wirklich :D
					log.debug("UnaryOperator [*]: Correct Type[isType:" + type
							+ "] [ExpectedType:nearly everything] at " + ((Token) seq.jjtGetValue()).beginLine);
				} else
				{
					LogMessages.logErrorMessageWithExcep("UnaryOperator [*]: Invalid Type[isType:" + type
							+ "] [ExpectedType:bool] at " + ((Token) seq.jjtGetValue()).beginLine);
				}
				break;
			default:
				LogMessages.logErrorMessageWithExcep("UnaryOperator [" + operator + "]: Unknown Operator[isType:"
						+ type + "] [ExpectedType:unknown] at " + ((Token) seq.jjtGetValue()).beginLine);
			}
		} else if (seq instanceof BRAN_AST_SelectionStatement)
		{
			final String type = pda.pop();
			if (type.equals("bool"))
			{
				log.debug("SelectionStatement [" + seq.jjtGetValue().toString() + "]: Correct Typ[isType:" + type
						+ "] [ExpectedType:bool] at " + ((Token) seq.jjtGetValue()).beginLine);
			} else
			{
				LogMessages.logErrorMessageWithExcep(
						"SelectionStatement [" + seq.jjtGetValue().toString() + "]: Incorrect Typ[isType:" + type
						+ "] [ExpectedType:bool] at " + ((Token) seq.jjtGetValue()).beginLine);
			}
		} else if (seq instanceof BRAN_AST_IterationStatement)
		{
			final String iterationType = seq.jjtGetValue().toString();
			// While Statement needs to be int
			if (iterationType.equals("while"))
			{
				final String type = pda.pop();
				if (type.equals("bool"))
				{
					log.debug("IterationStatement [" + iterationType + "]: Correct Typ[isType:" + type
							+ "] [ExpectedType:bool] at " + ((Token) seq.jjtGetValue()).beginLine);
				} else
				{
					LogMessages.logErrorMessageWithExcep(
							"IterationStatement [" + iterationType + "]: Incorrect Typ[isType:" + type
							+ "] [ExpectedType:bool] at " + ((Token) seq.jjtGetValue()).beginLine);
				}
			}
			// For Statement also needs to be bool
			else if (iterationType.equals("for"))
			{
				switch (seq.jjtGetNumChildren())
				{
				// for(; 5<10;)
				case 2:
					String type = pda.pop();
					if (type.equals("bool"))
					{
						log.debug("IterationStatement [" + iterationType + "]: Correct Typ[isType:," + type
								+ ",] [ExpectedType:,bool,] at " + ((Token) seq.jjtGetValue()).beginLine);
					} else
					{
						LogMessages.logErrorMessageWithExcep(
								"IterationStatement [" + iterationType + "]: Incorrect Typ[isType:" + type
								+ "] [ExpectedType:bool] at " + ((Token) seq.jjtGetValue()).beginLine);
					}
					break;
				case 3:
					type = pda.pop();
					String type2 = pda.pop();
					// for(INT; BOOL;)
					if (type.equals("bool") && type2.equals("int"))
					{
						log.debug("IterationStatement [" + iterationType + "]: Correct Types[isType:" + type2 + ","
								+ type + ",] [ExpectedType:int,bool,] at " + ((Token) seq.jjtGetValue()).beginLine);
					}
					// for(;BOOL;INT)
					else if (type.equals("int") && type2.equals("bool"))
					{
						log.debug("IterationStatement [" + iterationType + "]: Correct Types[isType:," + type2 + ","
								+ type + "] [ExpectedType:,bool,int] at " + ((Token) seq.jjtGetValue()).beginLine);
					} else
					{
						LogMessages.logErrorMessageWithExcep("IterationStatement [" + iterationType
								+ "]: Incorrect Typ[isType:" + type + "," + type2 + "] [ExpectedType:bool,int] at "
								+ ((Token) seq.jjtGetValue()).beginLine);
					}
					break;
				case 4:
					type = pda.pop();
					type2 = pda.pop();
					final String type3 = pda.pop();
					// for(INT;BOOL;INT)
					if (type.equals("int") && type2.equals("bool") && type3.equals("int"))
					{
						log.debug("IterationStatement [" + iterationType + "]: Correct Typ[isType:" + type + ","
								+ type2 + "," + type3 + "] [ExpectedType:int,bool,int] at "
								+ ((Token) seq.jjtGetValue()).beginLine);
					} else
					{
						LogMessages.logErrorMessageWithExcep("IterationStatement [" + iterationType
								+ "]: Incorrect Typ[isType:" + type + "," + type2 + "," + type3
								+ "] [ExpectedType:int,bool,int] at " + ((Token) seq.jjtGetValue()).beginLine);
					}
					break;
				default:
					LogMessages.logErrorMessageWithExcep("IterationStatement [" + iterationType
							+ "]: Incorrect Parameters[Parameters:" + (seq.jjtGetNumChildren() - 1)
							+ "] [ExpectedParameters:at least 1] at " + ((Token) seq.jjtGetValue()).beginLine);
				}
			}
			// Everything else is not valid but its better to return some error even
			// though the grammar should not allow anything else
			else
			{
				LogMessages.logErrorMessageWithExcep(
						"IterationStatement [" + iterationType + "]: Unknown Statement [isType:" + pda.pop()
						+ "] [ExpectedType:unknownType] at " + ((Token) seq.jjtGetValue()).beginLine);
			}
		}
	}

	/**
	 * Returns the number of pointers which are recursive childs from the Parent Pointer
	 *
	 * @param pntrs
	 * @param cnt
	 * @return
	 */
	private int cntNumPntrs(final BRAN_AST_Pointer pntrs)
	{
		int tmpCnt = 1;
		if (pntrs.jjtGetNumChildren() > 0)
		{
			tmpCnt += cntNumPntrs((BRAN_AST_Pointer) pntrs.jjtGetChild(0));
		}
		return tmpCnt;
	}

	/**
	 * prints this functions name, return type and variables. Parameters are printed seperately and
	 * within the normal symbols of this function as they are both
	 */
	public void dump()
	{
		log.debug("Function: {}", self);
		for (final Definition definition : params)
		{
			log.debug("\tParam: {}", definition);
		}
		for (final Definition definition : symbols.getSymbols())
		{
			log.debug("\tSymbol: {}", definition);
		}
	}

	/**
	 * Checks if the function for a specific identifier exists
	 */
	@SuppressWarnings("unused")
	private boolean functionExists(final String funcIdentifier)
	{
		try
		{
			Main.getFunctionHandler(funcIdentifier);
			return true;
		} catch (final Exception e)
		{
			return false;
		}
	}

	public ArrayList<String> getCode() throws IllegalAccessException
	{
		return codegenroot.getCode();
	}

	/**
	 * Get the definition of this function
	 *
	 * @return
	 */
	public Definition getFuncDef()
	{
		return self;
	}

	/**
	 * Get this functions name
	 *
	 * @return
	 */
	public String getFuncName()
	{
		return self.getName();
	}

	/**
	 * Get parameters
	 *
	 * @return
	 */
	public ArrayList<Definition> getParams()
	{
		return params;
	}

	/**
	 * Get parameter count
	 *
	 * @return
	 */
	public int getParamSize()
	{
		return params.size();
	}

	/**
	 * Get the return type of this function
	 *
	 * @return
	 */
	public DataType getReturnType()
	{
		return self.getType();
	}

	public BRAN_AST_Function getRoot()
	{
		return root;
	}

	/**
	 * Return variable from symbol table
	 */
	public Definition getVariable(final String name)
	{

		if (symbols.contains(name))
		{

			return symbols.get(name);
		}
		return null;
	}

	private SimpleNode handleArrayCall(final BRAN_AST_ArrayCall lv)
	{
		if (lv.jjtGetNumChildren() == 2) // as of current grammar
		{
			final SimpleNode ident = (SimpleNode) lv.jjtGetChild(0);
			// this is what needs to be done. but it requires all handled nodes
			// to be
			// stored and check before handling if node was already handled.
			// (currently
			// only for identifiers)
			handleNodes((SimpleNode) lv.jjtGetChild(1));
			return ident;
		}
		if (lv.jjtGetNumChildren() >= 1)
		{
			final SimpleNode c = (SimpleNode) lv.jjtGetChild(0);
			if (c.jjtGetValue() instanceof Token)
			{
				final Token t = ((Token) c.jjtGetValue()).next;
				log.error("Illegal ArrayCall at line {} col {}", t.beginLine, t.beginColumn);
			} else
			{
				log.error("Illegal ArrayCall with invalid nodes. Nodename: {}", c.jjtGetValue());
			}
		}
		return null;
	}

	/**
	 * Handle the body of this function which includes iterating through the body-nodes. If an
	 * action for a specific instance has been defined it will be processed. If, before this method
	 * is run, another class requested to be notified when this class is finished the created
	 * latches will be counted down signaling the waiting classes.
	 */
	public void handleBlock()
	{
		if (block == null)
		{
			log.fatal("function: {}: no block available", self);
		} else
		{
			if (!processed)
			{
				for (final Definition definition : params)
				{
					symbols.add(definition);
				}
				SimpleNode n = (SimpleNode) root.jjtGetChild(0);
				if (!(n instanceof BRAN_AST_FunctionDeclaration))
				{
					if (root.jjtGetNumChildren() > 1)
					{
						n = (SimpleNode) root.jjtGetChild(1);
					}
				}
				if ((n instanceof BRAN_AST_FunctionDeclaration))
				{
					codegenroot = new ControlNodeFunction((SimpleNode) root.jjtGetChild(0));
					codegenbuild = new CodeGenTreeBuilder(codegenroot);
					for (final VarsNode varsNode : codegenparams)
					{
						codegenroot.addParam(varsNode);
					}
					handleNodes(block);
					codegenroot.setSymbolTable(symbols);
					if (Main.GenerateCode) {
						handleNodesCodeGen(block);
					}
					// Signal latches
					if (latches != null)
					{
						for (final CountDownLatch latch : latches)
						{
							latch.countDown();
						}
					}
					processed = true; // last statement
				} else
				{
					throw new InvalidTreeStructureException(self.toString());
				}
			} else
			{
				log.error("This Function ({}) has already been processed", self);
			}
		}
	}

	/**
	 * Recursively called function to handle the nodes within the body.
	 *
	 * @since 06.06.17 - Variable Declarations are being processed extracting their child nodes for
	 *        type and name
	 * @param node
	 *            current node to process
	 */
	private void handleNodes(final SimpleNode node)
	{
		// TODO: handle function -> symbol table / typechecker / whatever
		if (node instanceof BRAN_AST_Sequence)
		{
			sequences.add((BRAN_AST_Sequence) node);
		}
		// Handling variable declarations
		if (node instanceof BRAN_AST_VariableDeclaration)
		{
			final SimpleNode type = (SimpleNode) node.jjtGetChild(0);
			final SimpleNode ident = (SimpleNode) node.jjtGetChild(1);
			Definition def = null;
			if (type instanceof BRAN_AST_ArrayDeclaration)
			{
				def = new Definition(ident.jjtGetValue().toString(),
						((SimpleNode) type.jjtGetChild(0)).jjtGetValue().toString());
				def.setArray(true);
			}
			// There are two identifiers for struct variables and we have to use
			// the
			// second one
			// struct x *s -> At this point s is the identifier not x because x
			// is the
			// type of our struct
			else if (type.jjtGetValue().toString().toLowerCase() == "struct")
			{
				try
				{
					// First identifier is x which represents the type of the
					// struct
					final BRAN_AST_Identifier structIdent = (BRAN_AST_Identifier) ((SimpleNode) type.jjtGetChild(0));
					final String structName = ((Token) structIdent.jjtGetValue()).image;

					// Pointer to structs are mandatory. Structs are not small.
					if (type.jjtGetParent().jjtGetNumChildren() > 2)
					{
						// Second identifier is s. This identifier should be
						// stored in the
						// symbol table.
						final BRAN_AST_Identifier structIdent2 = (BRAN_AST_Identifier) ((SimpleNode) type.jjtGetParent()
								.jjtGetChild(2));

						def = new Definition(structIdent2.jjtGetValue().toString(), DataType.STRUCT,
								((Token) node.jjtGetValue()).beginLine);
						def.loadStructFields(structName);
						def.setStruct(true);
						handledIdentifiers.add(structIdent);

						/*
						 * How many pointers do we have? There must be same number of pointers on
						 * both sides But on the left side there must be one more
						 */
						final BRAN_AST_Pointer numPointers = (BRAN_AST_Pointer) ((SimpleNode) node.jjtGetChild(1));
						def.setPointers(cntNumPntrs(numPointers));
					} else
					{
						LogMessages.logErrorMessageWithExcep("There is no pointer declaration to struct at line: "
								+ ((Token) node.jjtGetValue()).beginLine
								+ ". Type struct not small, insert a pointer to your struct identifier.");
					}
				} catch (final NullPointerException e)
				{
					LogMessages.logErrorMessageWithExcep("There is no struct definition like: struct "
							+ ((SimpleNode) type.jjtGetChild(0)).jjtGetValue() + ". Error at line: "
							+ ((Token) node.jjtGetValue()).beginLine);
				}
			} else
			{
				def = new Definition(ident.jjtGetValue().toString(), type.jjtGetValue().toString());
			}
			def.setVarScopeCounter(varScopeCounterVar.incrementAndGet())
			.setStartLine(((Token) ident.jjtGetValue()).beginLine);
			symbols.add(def);
		}
		if (node instanceof BRAN_AST_AllocOP)
		{
			if (node.jjtGetNumChildren() == 2)
			{
				if ((node.jjtGetChild(0) instanceof BRAN_AST_TypeSpecifier)
						&& (node.jjtGetChild(1) instanceof BRAN_AST_Identifier))
				{
					handledIdentifiers.add((SimpleNode) node.jjtGetChild(1));
				}
			}
			if ((node.jjtGetNumChildren() == 1) && (node.jjtGetChild(0) instanceof BRAN_AST_TypeSpecifier)
					&& (node.jjtGetChild(0).jjtGetNumChildren() == 1)
					&& (node.jjtGetChild(0).jjtGetChild(0) instanceof BRAN_AST_Identifier))
			{
				handledIdentifiers.add((SimpleNode) node.jjtGetChild(0).jjtGetChild(0));
			}
		}
		if (node instanceof BRAN_AST_LVStructureOptional)
		{
			// discern struct lv and field
			if (node.jjtGetNumChildren() == 2) // has to be
			{
				SimpleNode lv = (SimpleNode) node.jjtGetChild(0);
				SimpleNode fid = (SimpleNode) node.jjtGetChild(1);
				if (lv instanceof BRAN_AST_ArrayCall)
				{
					lv = handleArrayCall((BRAN_AST_ArrayCall) lv);
				}
				if (fid instanceof BRAN_AST_ArrayCall)
				{
					fid = handleArrayCall((BRAN_AST_ArrayCall) fid);
				}

				// if (symbols.contains(((Token) lv.jjtGetValue()).image)) {
				// final Definition structDef = symbols.get(((Token)
				// lv.jjtGetValue()).image);
				// if ((structDef.getType() == DataType.STRUCT)
				// && structDef.getStructDef().contains(((Token)
				// fid.jjtGetValue()).image)) {
				// final Token t = (Token) fid.jjtGetValue();
				// log.trace("Identifier {} on struct {} found at line/col
				// {}/{}", t.image,
				// structDef, t.beginLine,
				// t.beginColumn);
				// }
				// }
				handledIdentifiers.add(lv);
				handledIdentifiers.add(fid);
			} else
			{
				log.error("Struct access node without proper childnodes");
			}
		}
		// Handling identifiers and checking if they already are known in the
		// symbol table
		if ((node instanceof BRAN_AST_Identifier) && !handledIdentifiers.contains(node))
		{
			// TODO: complete what needs to be done here. the current log
			// outputs should
			// not stay
			final Token t = (Token) node.jjtGetValue();
			if (t.next.image == "(")
			{
				// func ident
				// log.info("kekse");

			} else
			{
				if (!handledIdentifiers.contains(node))
				{
					// var ident
					if (!symbols.check(((Token) node.jjtGetValue()).image))
					{
						log.error("Identifier {} at line/col {}/{} mentioned but not declared.", t.image, t.beginLine,
								t.beginColumn);
					} else
					{
						log.trace("Identifier {} found at line/col {}/{}", symbols.get(t.image), t.beginLine,
								t.beginColumn);
					}
				}
			}
			handledIdentifiers.add(node);
		}
		if (node.jjtGetNumChildren() > 0)
		{
			for (int i = 0; i < node.jjtGetNumChildren(); i++)
			{
				handleNodes((SimpleNode) node.jjtGetChild(i));
			}
		}
	}

	private void handleNodesCodeGen(final SimpleNode node)
	{
		if (node instanceof BRAN_AST_Sequence)
		{
			try
			{
				final BRAN_AST_Sequence s = (BRAN_AST_Sequence) node;
				codegenhandledsequences.add(s);
				codegenbuild.handleSequence(s, null);
			} catch (final Exception e)
			{
				log.catching(e);
			}
		}
		if (node.jjtGetNumChildren() > 0 && !(node instanceof BRAN_AST_SelectionStatement) 
				&& !(node instanceof BRAN_AST_IterationStatement))
		{
			for (int i = 0; i < node.jjtGetNumChildren(); i++)
			{
				handleNodesCodeGen((SimpleNode) node.jjtGetChild(i));
			}
		}
	}

	private void handleParams(final BRAN_AST_Parameter param)
	{
		if (param.jjtGetNumChildren() >= 1)
		{
			Definition def;
			// actual param
			if (param.jjtGetChild(0) instanceof BRAN_AST_ParameterDeclaration)
			{
				final BRAN_AST_ParameterDeclaration decl = (BRAN_AST_ParameterDeclaration) param.jjtGetChild(0);
				// Tree sections of structs are not equal to other param
				// declarations
				if (((SimpleNode) decl.jjtGetChild(0)).jjtGetValue().toString().equals("struct")
						&& (decl.jjtGetNumChildren() == 3))
				{
					// Is there realy a struct like this?
					final String structName = ((SimpleNode) decl.jjtGetChild(0).jjtGetChild(0)).jjtGetValue()
							.toString();
					if (Main.getStruct(structName) == null)
					{
						LogMessages.logErrorMessageWithExcep("There is no STRUCT like " + structName
								+ " which was defined as function parameter at line : " + getFuncDef().getStartline());
					}
					decl.jjtSetValue(((SimpleNode) decl.jjtGetChild(2)).jjtGetValue());
					def = new Definition(decl);
					def.setStruct(true);
					def.setAllocated(true);
				} else
				{
					if (decl.jjtGetValue() == null)
					{
						decl.jjtSetValue(((SimpleNode) decl.jjtGetChild(1)).jjtGetValue());
					}
					def = new Definition(decl);
					if (decl.jjtGetChild(0) instanceof BRAN_AST_ArrayDeclaration)
					{
						def.setArray(true);
						def.setAllocated(true);
					}
				}
				params.add(def.setVarScopeCounter(varScopeCounterParam.decrementAndGet()));
				switch (def.getType().toString())
				{
				case "BOOL":
					codegenparams.add(new VarsNodeBoolean(decl, def));
					break;
				case "INT":
					codegenparams.add(new VarsNodeInt(decl, def));
					break;
				case "STRUCT":
					codegenparams.add(new VarsNodeStruct(decl, def));
					break;
				default:
					throw new InvalidTreeStructureException(def.toString());
				}
			} else
			{
				log.error("Param-Node without ParamDeclaration");
			}
			// another param?
			if ((param.jjtGetNumChildren() >= 2) && (param.jjtGetChild(1) instanceof BRAN_AST_Parameter))
			{
				handleParams((BRAN_AST_Parameter) param.jjtGetChild(1));
			}
		} else
		{
			log.error("Param-Node without Child-Node");
		}
	}

	/**
	 * Check if the definition of this function is complete. If no block is specified we only got
	 * the header and still need the function
	 *
	 * @return
	 */
	public boolean isDefinitionComplete()
	{
		return block != null;
	}

	/**
	 * Check if this function has been handled. Which means it is checked for errors and if
	 * successfully completed can be properly executed.
	 *
	 * @return
	 */
	public boolean isProcessingComplete()
	{
		return processed;
	}

	/**
	 * Checks correct types for Assignments and Increments. Includes ASNOP, Increment and Decrement
	 *
	 * @param op
	 */
	private void lowAssignment(final SimpleNode op)
	{
		String left, right;

		switch (op.jjtGetValue().toString())
		{
		// The correct type for increment and decrement is only int
		case "++":
		case "--":
			final String tmp = pda.pop();
			if (!tmp.equals("int"))
			{
				LogMessages.logErrorMessageWithExcep(
						"LowAssignment -> Incorrect type for [" + op.jjtGetValue().toString() + "] [Type:" + tmp
						+ "],[ExpectedType:int] at " + ((Token) op.jjtGetValue()).beginLine);
			} else
			{
				// We still need the variables for our for statement
				if (op.jjtGetParent() instanceof BRAN_AST_IterationStatement)
				{
					pda.push(tmp);
				}
				log.debug("LowAssignment -> Correct type for [" + op.jjtGetValue().toString() + "] ["
						+ ((SimpleNode) op.jjtGetChild(0)).jjtGetValue().toString() + ":int] at "
						+ ((Token) op.jjtGetValue()).beginLine);
			}
			break;

			// Both left and right have to be the same type
		case "=":
			left = pda.pop();
			right = pda.pop();

			if (!left.equals(right))
			{
				LogMessages.logErrorMessageWithExcep(
						"LowAssignment -> Incorrect type for [" + op.jjtGetValue().toString() + "] [Value:"
								+ ((Token) op.jjtGetValue()).next.image + "],[Type:" + left + "],[ExpectedType:"
								+ right + "] at " + ((Token) op.jjtGetValue()).beginLine);
			} else
			{
				// We still need the variables for our for statement
				if (op.jjtGetParent() instanceof BRAN_AST_IterationStatement)
				{
					pda.push(left);
				}
				log.debug("LowAssignment -> Correct type for [" + op.jjtGetValue().toString() + "] [Value:"
						+ ((Token) op.jjtGetValue()).next.image + "],[Type:" + left + "],[ExpectedType:" + right
						+ "] at " + ((Token) op.jjtGetValue()).beginLine);
			}
			break;

			// Both sides have to be the type INT
		case "+=":
		case "-=":
		case "*=":
		case "/=":
		case "%=":
		case "&=":
		case "|=":
			left = pda.pop();
			right = pda.pop();
			if (!left.equals("int") || !right.equals("int"))
			{
				LogMessages.logErrorMessageWithExcep("LowAssignment -> Incorrect type for ["
						+ op.jjtGetValue().toString() + "] [Value:" + ((Token) op.jjtGetValue()).next.image
						+ "],[Type:" + left + "],[ExpectedType:int] at " + ((Token) op.jjtGetValue()).beginLine);
			} else
			{
				log.debug("LowAssignment -> Correct type for [" + op.jjtGetValue().toString() + "] [Value:"
						+ ((Token) op.jjtGetValue()).next.image + "],[Type:" + left + "],[ExpectedType:" + right
						+ "] at " + ((Token) op.jjtGetValue()).beginLine);
				// We still need the variables for our for statement
				if (op.jjtGetParent() instanceof BRAN_AST_IterationStatement)
				{
					pda.push(left);
				}
			}
			break;
		}
	}

	@Override
	public void run()
	{
		handleBlock();
		try
		{
			checkProto();
		} catch (final RootException ex)
		{
			ex.printStackTrace();
			System.exit(-123456);
		}
	}

	/**
	 * Pr�ft, ob �bergebener String dem Datentyp entspricht
	 *
	 * @param toCheck
	 * @return
	 */
	private boolean typeCheck(final String toCheck, final DataType expectedType)
	{
		try
		{
			Integer.parseInt(toCheck);
			if (expectedType.equals(DataType.INT))
			{
				return true;
			}
			return false;
		} catch (final Exception e)
		{
			if ((toCheck.equals("true") || toCheck.equals("false")) && expectedType.equals(DataType.BOOL))
			{
				return true;
			}
			return false;
		}
	}

	private DataType getType(final String toCheck)
	{
		try
		{
			Integer.parseInt(toCheck);
			return DataType.INT;
		} catch (final Exception e)
		{
			if ((toCheck.equals("true") || toCheck.equals("false")))
			{
				return DataType.BOOL;
			}
			return DataType.VOID;
		}
	}
}
