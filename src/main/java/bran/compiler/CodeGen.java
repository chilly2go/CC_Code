package bran.compiler;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CodeGen
{
	HashMap<String, FunctionHandler>	funcs;
	private static final Logger			log	= LogManager.getLogger();

	public CodeGen(HashMap<String, FunctionHandler> funcs)
	{
		this.funcs = funcs;
	}

	public void gen()
	{
		try
		{
			ArrayList<String> code = new ArrayList<>();
			// program code
			code.add("enter 4");
			code.add("alloc 1");
			code.add("mark");
			code.add("loadc _main");
			code.add("call");
			code.add("halt");
			// foreach func -> gen
			for (String funcname : funcs.keySet())
			{
				FunctionHandler func = funcs.get(funcname);
				code.addAll(func.getCode());
				int i = 0;
				// curently dumping it. writing to file when finished
				if (Main.Debug) for (String line : code)
				{
					log.trace("{}|{}", i++, line);
				}
			}
			Main.filepointer = Main.filepointer.replace(".c0", ".cma").replace("testfiles", "generated");
			PrintWriter writer = new PrintWriter(Main.filepointer, "UTF-8");
			for (String line : code)
			{
				if (!line.startsWith("bran.data.")) writer.println(line);
			}
			writer.close();

		} catch (IllegalAccessException e)
		{
			log.catching(e);
		} catch (FileNotFoundException e)
		{
			log.catching(e);
		} catch (UnsupportedEncodingException e)
		{
			log.catching(e);
		}
	}
}
