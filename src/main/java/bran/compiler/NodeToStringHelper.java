package bran.compiler;

import bran.parser.SimpleNode;
import bran.parser.Token;

public class NodeToStringHelper
{
	public static String getStringOutput(SimpleNode node)
	{
		if (node.jjtGetValue() != null)
		{
			Token t = (Token) node.jjtGetValue();
			return t.image + "(" + t.beginLine + "/" + t.beginColumn + ")";
		} else
		{
			return node.getClass().getName();
		}
	}
}
