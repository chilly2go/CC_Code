package bran.compiler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bran.data.structures.DataType;
import bran.exceptions.RootException;
import bran.parser.SimpleNode;
import bran.parser.Token;

public class LogMessages {
	private static final Logger log = LogManager.getLogger();

	/**
	 * Ausgabe mit Exceoption, falls ein Identifier nicht gefunden wurde
	 *
	 * @param seq
	 */
	public static void logUnknownIdentifierWithException(final SimpleNode seq) {
		log.error("Identifier [" + seq.jjtGetValue() + "] - Perhaps there was an unknown identifier detected at "
				+ ((Token) seq.jjtGetValue()).beginLine + ":" + ((Token) seq.jjtGetValue()).beginColumn);
		throw new RootException(
				"Identifier [" + seq.jjtGetValue() + "] - Perhaps there was an unknown identifier detected at "
						+ ((Token) seq.jjtGetValue()).beginLine + ":" + ((Token) seq.jjtGetValue()).beginColumn);
	}

	public static void logPushExpressionValue(final SimpleNode seq, final DataType type) {
		log.debug("PUSH: value [" + ((Token) seq.jjtGetValue()).image + ":" + type.toString().toLowerCase() + "], at "
				+ ((Token) seq.jjtGetValue()).beginLine + ":" + ((Token) seq.jjtGetValue()).beginColumn);
	}

	public static void logErrorMessageWithExcep(final String msg) throws RootException {
		log.error(msg);
		throw new RootException(msg);
	}
}
