package bran.compiler;

import java.util.HashSet;

import bran.parser.BRAN_AST_Function;
import bran.parser.BRAN_AST_StructDefinition;
import bran.parser.BRAN_AST_TranslationUnit;
import bran.parser.SimpleNode;
import bran.parser.Token;

public class ASTTraversal
{
	SimpleNode					root;
	public static final String	prefix					= "BRAN_AST_";
	Boolean						processed				= false;
	private HashSet<SimpleNode>	nodesWithinFunctions	= new HashSet<>();

	public ASTTraversal(SimpleNode root)
	{
		this(root, true);
	}

	public ASTTraversal(SimpleNode root, Boolean print)
	{
		this.root = root;
	}

	public void update()
	{
		dump(prefix, "", root, false);
	}

	public void dump()
	{
		dump(prefix, "", root, true);
	}

	/**
	 * Traversing through the tree, setting proper node values for injected tokens.
	 * <i>print</i> will be set to true which leads to printing the tree to system.out
	 * 
	 * @param prefix
	 * @param value
	 */
	public void dump(String prefix, SimpleNode node)
	{
		dump(prefix, "", node, true);
	}

	/**
	 * Entry point for AST dump function. single point to properly set if AST has been
	 * processed (missing information for injected nodes has been extracted)
	 * 
	 * @param prefix
	 * @param value
	 * @param node
	 * @param print
	 */
	public void dump(String prefix, Object value, SimpleNode node, Boolean print)
	{
		dumpAST(prefix, value, node, print);
		processed = true;
	}

	/**
	 * Traversing through the tree, setting proper node values for injected tokens. if
	 * <i>print</i> is true the current node will also be printed to system.out
	 * 
	 * @param prefix
	 * @param value
	 * @param print
	 */
	private void dumpAST(String prefix, Object value, SimpleNode node, Boolean print)
	{
		if (node instanceof BRAN_AST_Function || node instanceof BRAN_AST_StructDefinition)
		{
			nodesWithinFunctions.add(node);
		} else
		{
			if (node instanceof BRAN_AST_TranslationUnit)
			{
				// root node. nothing to be done here
			} else
			{
				if (checkParentWithinFunction(node))
				{
					// everything is fine
				} else
				{
					throw new IllegalArgumentException(
							"Node " + NodeToStringHelper.getStringOutput(node) + " not within Function or Struct");
				}
			}
		}
		if (print) System.out.println(node.toString(prefix) + " " + value);
		if (!processed && node instanceof BRAN_AST_Function)
		{
			Main.handleFunction((BRAN_AST_Function) node);
		}
		if (!processed && node instanceof BRAN_AST_StructDefinition)
		{
			Main.handleStruct((BRAN_AST_StructDefinition) node);
		}
		if (node.jjtGetNumChildren() > 0)
		{
			for (int i = 0; i < node.jjtGetNumChildren(); ++i)
			{
				SimpleNode n = (SimpleNode) node.jjtGetChild(i);
				if (n != null)
				{
					if (n.jjtGetValue() instanceof Token)
					{
						Token t = (Token) n.jjtGetValue();
						if (node.jjtGetValue() == null) // if the current node was
														 // injected to produce a proper
														 // AST
						{
							if (t.next != null) // check if next is a proper token
							{
								// copy essential fields (we want a copy and not a ref)
								Token t2 = new Token(t.next.kind, t.next.image);
								t2.beginColumn = t.beginColumn;
								t2.beginLine = t.beginLine;
								node.jjtSetValue(t2);
							}
						}
						dumpAST(prefix + "-",
								t.image + " (line: " + t.beginLine + " | col: " + t.beginColumn + "/" + t.endColumn
										+ " | kind: " + t.kind + " | next: " + (t.next == null ? "null" : t.next.image)
										+ ")",
								n, print);
					} else
					{
						dumpAST(prefix + "-", n.jjtGetValue(), n, print);
					}
				}
			}
		}
	}

	private boolean checkParentWithinFunction(SimpleNode node)
	{
		if (node.jjtGetParent() != null)
		{
			SimpleNode p = (SimpleNode) node.jjtGetParent();
			// not yet checking if parent is function. should already be handled es we are
			// going top -> down
			if (nodesWithinFunctions.contains(p)) // parent known?
			{
				nodesWithinFunctions.add(node); // add this node
				return true;
			} else
			{
				// parent unknown -> possible error message waiting around hte corner
				checkParentWithinFunction(p); // check for parent upwards the tree
				if (nodesWithinFunctions.contains(p)) // parent now known?
				{
					nodesWithinFunctions.add(node); // add node
					return true;
				} else
				{ // upwards failed
					return false;
				}
			}
		} else
			return false;
	}
}
