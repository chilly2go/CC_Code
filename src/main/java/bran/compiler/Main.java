package bran.compiler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bran.cma.CMA_Main;
import bran.data.structures.DataType;
import bran.data.structures.Definition;
import bran.data.structures.StructDef;
import bran.exceptions.RootException;
import bran.parser.BRAN_AST_Block;
import bran.parser.BRAN_AST_Function;
import bran.parser.BRAN_AST_StructDeclarationList;
import bran.parser.BRAN_AST_StructDefinition;
import bran.parser.BRAN_AST_TranslationUnit;
import bran.parser.C0Parser;

public class Main
{
	public static double							BranCompilerVersion			= 0.21;
	public static int								DefaultNodeChildNum			= 2;
	public static int								DefaultBufferTreeLeafs		= 7;
	public static boolean							DefaultTreeAddSelfToParent	= false;
	public static boolean							GenerateCode				= false;
	public static boolean							Debug						= false;
	private static final Logger						log							= LogManager.getLogger();
	private static HashMap<String, FunctionHandler>	funcs						= new HashMap<>();
	private static HashMap<String, StructDef>		structs						= new HashMap<>();
	private static HashMap<Definition, Integer>		varCounters					= new HashMap<>();
	private static AtomicInteger					varCounter					= new AtomicInteger(0);
	public static String							filepointer					= "";

	public static void main(final String[] args) throws FileNotFoundException
	{
		boolean helpTriggered = false;
		C0Parser parser;
		final Options options = new Options();
		options.addOption("h", "help", false, "Hilfe anzeigen");
		options.addOption("v", "version", false, "Version anzeigen");
		options.addOption("c", "code", false, "Code generieren");
		final Option inputOpt = new Option("i", "input", true, "Zu compilierende Datei");
		// inputOpt.setRequired(true);
		inputOpt.setRequired(false);
		options.addOption(inputOpt);
		// options.addOption("i", "input", true, "Zu compilierende Datei");
		final CommandLineParser cmdParser = new DefaultParser();
		try
		{
			final CommandLine cmd = cmdParser.parse(options, args);
			if (cmd.hasOption("h"))
			{
				helpTriggered = true;
			}
			if (cmd.hasOption("c"))
			{
				Main.GenerateCode = true;
			}
			if (cmd.hasOption("v"))
			{
				log.info("Version: {}", BranCompilerVersion);
			}
			if (cmd.hasOption("i"))
			{
				filepointer = cmd.getOptionValue("i");
			} else
			{
				helpTriggered = true;
			}
		} catch (final ParseException e)
		{
			helpTriggered = true;
			log.catching(e);
		}
		if (helpTriggered)
		{
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("C0 Parser", options);
		} else
		{
			final File input = new File(filepointer);
			if (input.exists() && !input.isDirectory())
			{
				if (input.canRead())
				{
					log.info("Parsing inputfile: {}", filepointer);
					parser = new C0Parser(new java.io.FileInputStream(input));
					try
					{
						@SuppressWarnings("static-access")
						final BRAN_AST_TranslationUnit expr = parser.TranslationUnit();
						log.trace("Filling injected nodes with specifiers");
						final ASTTraversal traversal = new ASTTraversal(expr);
						traversal.update();
						log.info("Printing AST");
						if (Main.Debug) traversal.dump();
						FunctionHandler mainFunc = null;
						final ExecutorService exec = Executors
								.newFixedThreadPool(Runtime.getRuntime().availableProcessors() / 2);
						for (final FunctionHandler func : funcs.values())
						{
							exec.execute(func);
							if (func.getFuncName().equals("main"))
							{
								mainFunc = func;
							}
						}
						try
						{
							Thread.sleep(500);
							exec.shutdown();
							while (!exec.isTerminated())
							{
								exec.awaitTermination(10, TimeUnit.MINUTES);
								if (!exec.isTerminated()) log.info("Still waiting ");
							}
						} catch (final InterruptedException e)
						{
							log.catching(e);
						}
						if (mainFunc == null)
						{
							try
							{
								LogMessages.logErrorMessageWithExcep("No [main()] Function found ");
							} catch (final RootException ex)
							{
								ex.printStackTrace();
								System.exit(-12345);
							}
						} else
						{
							if ((mainFunc != null) && !mainFunc.getReturnType().equals(DataType.INT))
							{
								try
								{
									LogMessages.logErrorMessageWithExcep("[main()] Function invalid return [ReturnType:"
											+ mainFunc.getReturnType().toString().toLowerCase()
											+ "] [ExpectedType:int]");
								} catch (final RootException ex)
								{
									ex.printStackTrace();
									System.exit(-12345);
								}
							}
						}
						log.debug("Dumping collected structs: ");
						for (final StructDef struct : structs.values())
						{
							struct.dump();
						}
						log.debug("Dumping collected functions: ");
						for (final FunctionHandler func : funcs.values())
						{
							func.dump();
						}
						if (Main.GenerateCode)
						{
							try
							{
								log.debug("Dumping code: ");
								CodeGen cg = new CodeGen(funcs);
								cg.gen();
								CMA_Main cma = new CMA_Main(Main.filepointer);
								cma.run();
							} catch (Exception ex)
							{
								log.catching(ex);
								System.exit(-123456);
							}
						} else
						{
							log.debug("CodeGen disabled");
						}
					} catch (bran.parser.ParseException e)
					{
						log.catching(e);
					}
				} else
				{
					log.error("Inputfile ({}) can not be read!", input.getName());
				}
			} else
			{
				log.error("Inputfile ({}) does not exist or is directory", filepointer);
			}
		}
	}

	public static void handleFunction(final BRAN_AST_Function func)
	{
		final FunctionHandler handler = new FunctionHandler(func);
		if (funcs.containsKey(handler.getFuncName())) // function already known
		{
			if (func.jjtGetNumChildren() >= 2)
			{
				// child, if
				// present
				if (func.jjtGetChild(1) instanceof BRAN_AST_Block)
				{
					funcs.get(handler.getFuncName()).addFuncBody((BRAN_AST_Block) func.jjtGetChild(1));
				} else
				{
					log.error("second appearance of function but no function body or improper tree structure");
				}
			} else
			{
				log.error("second appearance of function but not enough children (no body?!)");
			}
		} else // currently unknown function
		{
			funcs.put(handler.getFuncName(), handler);
		}
	}

	/**
	 * If a CountDownLatch is requested for a known function it will be created, added to
	 * the function and returned to be able to wait for its completion. A CountDownLatch
	 * which was initialized with a count of 0 will be created if the function is not
	 * known or the function has already completed processing. Waiting on a Latch with a
	 * count of 0 results in no wait.
	 *
	 * @param functionName
	 *            function name the latch is requested for
	 * @return A CountDownLatch initialized with a count of 1 if the requested function
	 *         was known.
	 */
	public static CountDownLatch requestLatch(final String functionName)
	{
		CountDownLatch latch = new CountDownLatch(0);
		if (funcs.containsKey(functionName))
		{
			final FunctionHandler func = funcs.get(functionName);
			if (!func.isProcessingComplete())
			{
				latch = new CountDownLatch(1);
				funcs.get(functionName).addLatch(latch);
			}
		}
		return latch;
	}

	public static Integer getDefinitionCounter(final Definition def)
	{
		varCounters.put(def, varCounter.incrementAndGet());
		return varCounters.get(def);
	}

	public static StructDef getStruct(final String structName)
	{
		if (structs.containsKey(structName))
		{
			return structs.get(structName);
		} else
		{
			return null;
		}
	}

	/*
	 * Behandelt das Parsen von Structs. Es gibt drei Formen: Struct-Header, Struct mit
	 * Body jedoch aber leer und die volle Struct-Definition.
	 */
	public static void handleStruct(final BRAN_AST_StructDefinition node)
	{
		final StructDef s = new StructDef(node);
		s.updateSelfDefinition();
		if (structs.containsKey(s.getName()))
		{

			if ((node.jjtGetNumChildren() == 2) && !structs.get(s.getName()).isDefinitionComplete())
			{

				structs.get(s.getName()).addBlock((BRAN_AST_StructDeclarationList) node.jjtGetChild(1));
			} else
			{

				throw new IllegalStateException("Struct (" + s.getDefinition().toString() + ") already defined ("
						+ structs.get(s.getName()).getDefinition().toString() + ")");
			}
		} else
		{
			structs.put(s.getName(), s);
		}
	}

	/**
	 * Returns the function handler for a given function name. This step is necessary
	 * because the type checker has to check the number of params a called function has.
	 *
	 * @param funcName
	 */
	public static Definition getVariable(final String functionName, final String variableName)
	{

		return funcs.get(functionName).getVariable(variableName);
	}

	public static FunctionHandler getFunctionHandler(final String funcName)
	{
		return funcs.get(funcName);
	}
}
