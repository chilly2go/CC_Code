package bran.compiler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bran.data.structures.DataType;
import bran.data.structures.Definition;
import bran.data.structures.tree.ControlNodeAlloc;
import bran.data.structures.tree.ControlNodeAssign;
import bran.data.structures.tree.ControlNodeBlock;
import bran.data.structures.tree.ControlNodeConditional;
import bran.data.structures.tree.ControlNodeConditionalIf;
import bran.data.structures.tree.ControlNodeFunction;
import bran.data.structures.tree.ControlNodeFunctionCall;
import bran.data.structures.tree.ControlNodeLoop;
import bran.data.structures.tree.ControlNodeReturn;
import bran.data.structures.tree.NodeTypes;
import bran.data.structures.tree.OperatorsNodeAndOr;
import bran.data.structures.tree.OperatorsNodeCalc;
import bran.data.structures.tree.OperatorsNodeComp;
import bran.data.structures.tree.TreeNode;
import bran.data.structures.tree.VarsNode;
import bran.data.structures.tree.VarsNodeArray;
import bran.data.structures.tree.VarsNodeBoolean;
import bran.data.structures.tree.VarsNodeConstant;
import bran.data.structures.tree.VarsNodeInt;
import bran.exceptions.InvalidTreeStructureException;
import bran.parser.BRAN_AST_AddSubOptional;
import bran.parser.BRAN_AST_AllocOP;
import bran.parser.BRAN_AST_ArrayCall;
import bran.parser.BRAN_AST_ArrayIndex;
import bran.parser.BRAN_AST_Block;
import bran.parser.BRAN_AST_ComparisonOptional;
import bran.parser.BRAN_AST_EqualsOptional;
import bran.parser.BRAN_AST_ExpressionValues;
import bran.parser.BRAN_AST_FullAssignment;
import bran.parser.BRAN_AST_FunctionCall;
import bran.parser.BRAN_AST_Identifier;
import bran.parser.BRAN_AST_IterationStatement;
import bran.parser.BRAN_AST_LogicalAndOptional;
import bran.parser.BRAN_AST_LogicalOrOptional;
import bran.parser.BRAN_AST_LowAssignOrIncrement;
import bran.parser.BRAN_AST_MultDivModOptional;
import bran.parser.BRAN_AST_Pointer;
import bran.parser.BRAN_AST_ReturnStatement;
import bran.parser.BRAN_AST_SelectionStatement;
import bran.parser.BRAN_AST_SelectionStatementOptional;
import bran.parser.BRAN_AST_Sequence;
import bran.parser.BRAN_AST_TypeSpecifier;
import bran.parser.BRAN_AST_VariableDeclaration;
import bran.parser.SimpleNode;
import bran.parser.Token;

public class CodeGenTreeBuilder
{
	private static final Logger	log			= LogManager.getLogger();
	ControlNodeFunction			root;
	char[]						alphabet	= "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	int							counter		= 0;

	public CodeGenTreeBuilder(ControlNodeFunction root)
	{
		this.root = root;
	}

	private OperatorsNodeCalc getIncDec(SimpleNode n)
	{
		OperatorsNodeCalc onc = new OperatorsNodeCalc(n);
		SimpleNode ident = (SimpleNode) n.jjtGetChild(0);
		onc.addChild(handleOpBranch(ident));
		onc.addChild(new VarsNodeConstant(n, 1));
		if (onc.getNumChilds() != 2)
		{
			Token t = (Token) ident.jjtGetValue();
			throw new InvalidTreeStructureException(
					"Error at " + t.beginLine + " " + t.beginColumn + "(" + t.image + ")");
		}
		return onc;
	}

	private VarsNode getVarNode(BRAN_AST_Identifier left)
	{
		VarsNode vn = null;
		Definition ldef = root.getSymbolTable().get(((Token) left.jjtGetValue()).image);
		if (ldef.getType() == DataType.BOOL)
		{
			vn = new VarsNodeBoolean(left, ldef);
		}
		if (ldef.getType() == DataType.INT)
		{
			vn = new VarsNodeInt(left, ldef);
		}
		return vn;
	}

	private VarsNodeArray handleArrayCall(BRAN_AST_ArrayCall n)
	{

		// as to be identifier / stuct + field
		SimpleNode left = (SimpleNode) n.jjtGetChild(0);
		VarsNodeArray vn = null;
		if (left instanceof BRAN_AST_Identifier)
		{
			Definition ldef = root.getSymbolTable().get(((Token) left.jjtGetValue()).image);
			vn = new VarsNodeArray(left, ldef);
		}
		// has to give an int for index
		SimpleNode right = (SimpleNode) n.jjtGetChild(1);
		if (right instanceof BRAN_AST_ArrayIndex)
		{
			right = (SimpleNode) right.jjtGetChild(0);
			vn.addChild(handleOpBranch(right));
		}
		return vn;
	}

	private void handleFuncCallParams(SimpleNode paramslist, ControlNodeFunctionCall cnfc)
	{
		for (int i = 0; i < paramslist.jjtGetNumChildren(); i++)
		{
			SimpleNode param = (SimpleNode) paramslist.jjtGetChild(i);
			cnfc.addChild(handleOpBranch(param));
		}
	}

	private void handleOp(SimpleNode node, TreeNode on)
	{
		if (node.jjtGetNumChildren() == 2)
		{
			SimpleNode left = (SimpleNode) node.jjtGetChild(0);
			SimpleNode right = (SimpleNode) node.jjtGetChild(1);
			on.addChild(handleOpBranch(left));
			on.addChild(handleOpBranch(right));
		} else if (node.jjtGetNumChildren() == 1)
		{
			on.addChild(handleOpBranch((SimpleNode) node.jjtGetChild(0)));
		} else
		{
			log.fatal("Operator node with wrong structure");
		}
	}

	private TreeNode handleOpBranch(SimpleNode node)
	{
		TreeNode tn = null;
		if (node instanceof BRAN_AST_VariableDeclaration)
		{
			// var alloc? struct will be followed by alloc. array will have
			// alloc_array
			if (node.jjtGetNumChildren() > 2 && node.jjtGetChild(2) instanceof BRAN_AST_Identifier)
			{
				node = (SimpleNode) node.jjtGetChild(2);
			} else if (node.jjtGetChild(1) instanceof BRAN_AST_Identifier)
			{
				node = (SimpleNode) node.jjtGetChild(1);
			} else
			{
				throw new InvalidTreeStructureException("VariableDeclaration does not contain Identifier. Line: "
						+ ((Token) node.jjtGetValue()).beginLine);
			}
		}
		if (node instanceof BRAN_AST_Identifier)
		{
			tn = getVarNode((BRAN_AST_Identifier) node);
		}
		if (node instanceof BRAN_AST_ExpressionValues)
		{
			tn = (new VarsNodeConstant(node, Integer.valueOf(((Token) node.jjtGetValue()).image)));
		}
		if (node instanceof BRAN_AST_AddSubOptional || node instanceof BRAN_AST_MultDivModOptional)
		{
			tn = new OperatorsNodeCalc(node);
			handleOp(node, (OperatorsNodeCalc) tn);
		}
		if (node instanceof BRAN_AST_EqualsOptional || node instanceof BRAN_AST_ComparisonOptional)
		{
			tn = new OperatorsNodeComp(node);
			tn.addChild(handleOpBranch((SimpleNode) node.jjtGetChild(0)));
			tn.addChild(handleOpBranch((SimpleNode) node.jjtGetChild(1)));
		}
		if (node instanceof BRAN_AST_LogicalOrOptional || node instanceof BRAN_AST_LogicalAndOptional)
		{
			tn = new OperatorsNodeAndOr(node);
			checkIf3(node, tn);
		}
		if (node instanceof BRAN_AST_ArrayCall)
		{
			tn = handleArrayCall((BRAN_AST_ArrayCall) node);
		}
		if (node instanceof BRAN_AST_FunctionCall)
		{
			tn = new ControlNodeFunctionCall((SimpleNode) node.jjtGetChild(0));
			handleFuncCallParams((SimpleNode) node.jjtGetChild(1), (ControlNodeFunctionCall) tn);
		}
		if (node instanceof BRAN_AST_AllocOP)
		{
			tn = new ControlNodeAlloc(node);
			tn.addChild(handleOpBranch((SimpleNode) node.jjtGetChild(1)));
			if (node.jjtGetChild(0) instanceof BRAN_AST_ExpressionValues)
			{
				((ControlNodeAlloc) tn).setAllocSize(
						Integer.valueOf(((Token) ((SimpleNode) node.jjtGetChild(0)).jjtGetValue()).image));
			}
			if (node.jjtGetChild(0) instanceof BRAN_AST_TypeSpecifier)
			{
				String type = (((Token) ((SimpleNode) node.jjtGetChild(0)).jjtGetValue()).image).toUpperCase();
				if (NodeTypes.valueOf(type) == NodeTypes.BOOLEAN || NodeTypes.valueOf(type) == NodeTypes.INT)
					((ControlNodeAlloc) tn).setAllocSize(1);
			}
		}
		if (node instanceof BRAN_AST_SelectionStatement)
		{
			if (node.jjtGetNumChildren() > 0)
			{
				ControlNodeConditional cnc = new ControlNodeConditional(node);
				counter++;
				cnc.setJumpsymbol("_".concat(root.getName()).concat("_").concat(String.valueOf(counter)));
				/*
				 * 2 children = condition + then 3 children = condition + then + else
				 */
				// CONDITION
				cnc.setCondition(handleOpBranch((SimpleNode) node.jjtGetChild(0)));
				// if has to have at least 2 children
				if (node.jjtGetChild(1) instanceof BRAN_AST_Sequence)
				{
					SimpleNode n = (SimpleNode) node.jjtGetChild(1); // sequence
					if (n.jjtGetChild(0) instanceof BRAN_AST_Block)
					{
						n = (SimpleNode) n.jjtGetChild(0);
						for (int i = 0; i < n.jjtGetNumChildren(); i++)
						{
							if (n.jjtGetChild(i) instanceof BRAN_AST_Sequence)
							{
								cnc.addChild(
										handleOpBranch((SimpleNode) ((SimpleNode) n.jjtGetChild(i)).jjtGetChild(0)));
							}
						}
					}
				}
				if (node.jjtGetNumChildren() == 3)
				{
					// else part
					if (node.jjtGetChild(2) instanceof BRAN_AST_SelectionStatementOptional)
					{
						SimpleNode n = (SimpleNode) node.jjtGetChild(2); // else
						n = (SimpleNode) n.jjtGetChild(0); // sequence
						n = (SimpleNode) n.jjtGetChild(0); // block
						for (int i = 0; i < n.jjtGetNumChildren(); i++)
						{
							if (n.jjtGetChild(i) instanceof BRAN_AST_Sequence)
							{
								cnc.addElseblock(
										handleOpBranch((SimpleNode) ((SimpleNode) n.jjtGetChild(i)).jjtGetChild(0)));
							}
						}
					}
				}
				tn = cnc;
			}
		}
		if (node instanceof BRAN_AST_LowAssignOrIncrement)
		{
			if (((Token) node.jjtGetValue()).image.equals("="))
			{
				// assignment
				tn = new ControlNodeAssign(node);
				// var to assign to
				tn.addChild(handleOpBranch((SimpleNode) node.jjtGetChild(0)));
				// expression to be assigned to
				tn.addChild(handleOpBranch((SimpleNode) node.jjtGetChild(1)));
			} else
			{
				SimpleNode ident = (SimpleNode) node.jjtGetChild(0);
				tn = new ControlNodeAssign(node);
				tn.addChild(handleOpBranch(ident));
				OperatorsNodeCalc onc = getIncDec((BRAN_AST_LowAssignOrIncrement) node);
				tn.addChild(onc);
			}
		}
		if (tn == null)
		{
			log.info("TreeNode is null");
		}
		return tn;
	}

	private void checkIf(SimpleNode n, TreeNode cnc)
	{
		SimpleNode left = (SimpleNode) n.jjtGetChild(0);
		checkIf2(left, cnc);
		SimpleNode right = (SimpleNode) n.jjtGetChild(1);
		checkIf2(right, cnc);
	}

	private void checkIf2(SimpleNode c, TreeNode cnc)
	{
		// ==, !=, >, <, >=, <=
		if (c instanceof BRAN_AST_EqualsOptional || c instanceof BRAN_AST_ComparisonOptional)
		{
			OperatorsNodeComp oc = new OperatorsNodeComp(c);
			cnc.addChild(oc);

			TreeNode tl = handleOpBranch((SimpleNode) c.jjtGetChild(0));
			TreeNode tr = handleOpBranch((SimpleNode) c.jjtGetChild(1));

			if (tl == null)
			{
				if (c.jjtGetChild(0) instanceof BRAN_AST_MultDivModOptional)
				{
					OperatorsNodeCalc onc = new OperatorsNodeCalc((SimpleNode) c.jjtGetChild(0));
					handleOp((BRAN_AST_MultDivModOptional) c.jjtGetChild(0), onc);
					oc.addChild(onc);
				}
			}

			if (tr == null)
			{
				if (c.jjtGetChild(1) instanceof BRAN_AST_MultDivModOptional)
				{
					OperatorsNodeCalc onc = new OperatorsNodeCalc((SimpleNode) c.jjtGetChild(1));
					handleOp((BRAN_AST_MultDivModOptional) c.jjtGetChild(1), onc);
					oc.addChild(onc);
				}
			}

			oc.addChild(tl);
			oc.addChild(tr);
		}
		// ||, &&, ^
		if (c instanceof BRAN_AST_LogicalOrOptional || c instanceof BRAN_AST_LogicalAndOptional)
		{
			OperatorsNodeAndOr oc = new OperatorsNodeAndOr(c);
			cnc.addChild(oc);
			checkIf(c, oc);
		}
	}

	private void checkIf3(SimpleNode n, TreeNode cnc)
	{
		SimpleNode left = (SimpleNode) n.jjtGetChild(0);
		cnc.addChild(handleOpBranch(left));
		if (n.jjtGetNumChildren() >= 2)
		{
			SimpleNode right = (SimpleNode) n.jjtGetChild(1);
			cnc.addChild(handleOpBranch(right));
		}
	}

	// private void checkIf2(SimpleNode c, TreeNode cnc)
	// {
	// // ==, !=, >, <, >=, <=
	// if (c instanceof BRAN_AST_EqualsOptional || c instanceof
	// BRAN_AST_ComparisonOptional)
	// {
	// OperatorsNodeComp oc = new OperatorsNodeComp(c);
	// cnc.addChild(oc);
	//
	// TreeNode tl = handleOpBranch((SimpleNode) c.jjtGetChild(0));
	// TreeNode tr = handleOpBranch((SimpleNode) c.jjtGetChild(1));
	//
	// if (tl == null)
	// {// should never reach this point
	// log.fatal("should not have gotten here");
	// if (c.jjtGetChild(0) instanceof BRAN_AST_MultDivModOptional)
	// {
	// OperatorsNodeCalc onc = new OperatorsNodeCalc((SimpleNode) c.jjtGetChild(0));
	// handleOp((BRAN_AST_MultDivModOptional) c.jjtGetChild(0), onc);
	// oc.addChild(onc);
	// }
	// }
	//
	// if (tr == null)
	// {// should never reach this point
	// log.fatal("should not have gotten here");
	// if (c.jjtGetChild(1) instanceof BRAN_AST_MultDivModOptional)
	// {
	// OperatorsNodeCalc onc = new OperatorsNodeCalc((SimpleNode) c.jjtGetChild(1));
	// handleOp((BRAN_AST_MultDivModOptional) c.jjtGetChild(1), onc);
	// oc.addChild(onc);
	// }
	// }
	//
	// oc.addChild(tl);
	// oc.addChild(tr);
	// }
	// // ||, &&, ^
	// if (c instanceof BRAN_AST_LogicalOrOptional || c instanceof
	// BRAN_AST_LogicalAndOptional)
	// {
	// OperatorsNodeAndOr oc = new OperatorsNodeAndOr(c);
	// cnc.addChild(oc);
	// checkIf(c, oc);
	// }
	// }

	private void setParentBlock(TreeNode parent, TreeNode child)
	{
		if (parent != null)
		{
			parent.addChild(child);
			child.setParent(parent);
		} else
			root.addChild(child);
	}

	public CodeGenTreeBuilder handleSequence(SimpleNode s, TreeNode parent)
	{
		int labelIdx = 0;
		if (root.getNumChilds() > 0)
		{
			if (root.getChildAt(root.getNumChilds() - 1) instanceof ControlNodeConditionalIf
					|| root.getChildAt(root.getNumChilds() - 1) instanceof ControlNodeLoop)
			{
				labelIdx = counter;
			}
		}

		SimpleNode n;
		if (s instanceof BRAN_AST_Sequence)
		{
			n = (SimpleNode) s.jjtGetChild(0);
		} else
		{
			n = s;
		}
		if (s.jjtGetNumChildren() > 0)
		{
			if (n instanceof BRAN_AST_FunctionCall)
			{
				TreeNode tn = handleOpBranch(n);
				root.addChild(tn);
			}
			if (n instanceof BRAN_AST_FullAssignment)
			{
				if (n.jjtGetNumChildren() == 2)
				{
					log.info("assign u");
					ControlNodeAssign cna = new ControlNodeAssign(n);

					if (labelIdx > 0)
					{
						cna.setLabel(String.valueOf(alphabet[counter]));
					}
					handleOp(n, cna);
					if (cna.getNumChilds() != 2)
					{
						log.error("error within tree");
					}
					setParentBlock(parent, cna);
				} else
				{
					log.error("Assignment missing nodes");
				}
			}
			if (n instanceof BRAN_AST_ReturnStatement)
			{
				if (n.jjtGetNumChildren() == 1)
				{
					ControlNodeReturn cnr = new ControlNodeReturn(n);
					handleOp(n, cnr);
					if (labelIdx > 0)
					{
						cnr.setLabel(String.valueOf(alphabet[counter]));
					}
					setParentBlock(parent, cnr);
				}
			}
			if (n instanceof BRAN_AST_LowAssignOrIncrement)
			{
				if (((Token) n.jjtGetValue()).image.equals("="))
				{
					// assignment
					ControlNodeAssign cna = new ControlNodeAssign(n);

					if (labelIdx > 0)
					{
						cna.setLabel(String.valueOf(alphabet[counter]));
					}

					// var to assign to
					SimpleNode left = (SimpleNode) n.jjtGetChild(0);
					cna.addChild(handleOpBranch(left));
					// expression to be assigned to
					SimpleNode right = (SimpleNode) n.jjtGetChild(1);
					cna.addChild(handleOpBranch(right));
					setParentBlock(parent, cna);
				} else
				{
					SimpleNode ident = (SimpleNode) n.jjtGetChild(0);
					ControlNodeAssign cna = new ControlNodeAssign(n);
					cna.addChild(handleOpBranch(ident));
					OperatorsNodeCalc onc = getIncDec((BRAN_AST_LowAssignOrIncrement) n);
					cna.addChild(onc);
					setParentBlock(parent, cna);
				}
			}

			if (n instanceof BRAN_AST_SelectionStatement)
			{
				TreeNode cnc = handleOpBranch(n);
				// if (n.jjtGetNumChildren() > 0)
				// {
				// ControlNodeConditionalIf cnc = new ControlNodeConditionalIf(n);
				/*
				 * 2 children = condition + then 3 children = condition + then + else
				 */
				// CONDITION
				// checkIf(n, cnc);
				// if has to have at least 2 children
				// if(n.jjtGetNumChildren() == 3)
				// {
				//
				// }
				// if (labelIdx > 0)
				// {
				// cnc.setLabel(String.valueOf(alphabet[counter]));
				// }
				//
				// // CONDITION
				// checkIf(n, cnc);
				//
				// // BLOCK 1 - IF
				// SimpleNode astBlockIf = (SimpleNode) n.jjtGetChild(1).jjtGetChild(0);
				//
				// // Block unter if setzen
				// ControlNodeBlock cbIf = new ControlNodeBlock((SimpleNode)
				// n.jjtGetChild(1).jjtGetChild(0));
				// cnc.addChild(cbIf);
				// cbIf.setParent(cnc);
				//
				// cbIf.setLabel(String.valueOf(alphabet[counter]));
				// counter++;
				//
				// // Kinder unter Block setzen
				// for (int i = 0; i < astBlockIf.jjtGetNumChildren(); i++)
				// {
				// BRAN_AST_Sequence astSeqIf = (BRAN_AST_Sequence)
				// astBlockIf.jjtGetChild(i);
				// handleSequence(astSeqIf, cbIf);
				// }
				//
				// // BLOCK 2 - ELSE
				// if (n.jjtGetNumChildren() == 3)
				// {
				// SimpleNode sn3 = (SimpleNode) n.jjtGetChild(2);
				//
				// // Block unter if setzen
				// ControlNodeBlock cbElse = new ControlNodeBlock((SimpleNode)
				// n.jjtGetChild(2).jjtGetChild(0));
				// cnc.addChild(cbElse);
				// cbIf.setParent(cnc);
				//
				// cbElse.setLabel(String.valueOf(alphabet[counter]));
				// counter++;
				//
				// cnc.setJump(counter);
				//
				// // Kinder unter Block setzen
				// BRAN_AST_Block astBlockElse = (BRAN_AST_Block)
				// sn3.jjtGetChild(0).jjtGetChild(0);
				//
				// for (int i = 0; i < astBlockElse.jjtGetNumChildren(); i++)
				// {
				// BRAN_AST_Sequence astSeqElse = (BRAN_AST_Sequence)
				// astBlockElse.jjtGetChild(i);
				// handleSequence(astSeqElse, cbElse);
				// }
				// } else
				// {
				// counter++;
				// cnc.setJump(counter);
				// }
				root.addChild(cnc);
				log.info(cnc);
				// }

			}
			if (n instanceof BRAN_AST_IterationStatement)
			{
				if (n.jjtGetNumChildren() > 0)
				{

					ControlNodeLoop cnl = new ControlNodeLoop(n);

					cnl.setLabel(String.valueOf(alphabet[counter]));
					counter++;

					if (n.jjtGetValue().toString().equals("while"))
					{
						SimpleNode bedingung = (SimpleNode) n.jjtGetChild(0);
						// Bedingung
						checkIf2(bedingung, cnl);

						// Block
						SimpleNode astBlock = (SimpleNode) n.jjtGetChild(1).jjtGetChild(0);

						ControlNodeBlock cbBlock = new ControlNodeBlock(astBlock);
						cnl.addChild(cbBlock);
						cbBlock.setParent(cnl);

						cbBlock.setLabel(String.valueOf(alphabet[counter]));
						counter++;

						cnl.setJump(counter);
						// Kinder unter Block setzen
						for (int i = 0; i < astBlock.jjtGetNumChildren(); i++)
						{
							BRAN_AST_Sequence astSeqIf = (BRAN_AST_Sequence) astBlock.jjtGetChild(i);
							handleSequence(astSeqIf, cbBlock);
						}
					} else
					{
						// Kind 1
						handleSequence((SimpleNode) n.jjtGetChild(0), null);

						// Kind 2 Bedingung
						SimpleNode bedingung = (SimpleNode) n.jjtGetChild(1);
						checkIf2(bedingung, cnl);

						// Kind 4 Block
						SimpleNode astBlock = (SimpleNode) n.jjtGetChild(3).jjtGetChild(0);

						ControlNodeBlock cbBlock = new ControlNodeBlock(astBlock);
						cnl.addChild(cbBlock);
						cbBlock.setParent(cnl);

						cbBlock.setLabel(String.valueOf(alphabet[counter]));
						counter++;

						cnl.setJump(counter);

						// Kinder unter Block setzen
						for (int i = 0; i < astBlock.jjtGetNumChildren(); i++)
						{
							BRAN_AST_Sequence astSeqIf = (BRAN_AST_Sequence) astBlock.jjtGetChild(i);
							handleSequence(astSeqIf, cbBlock);
						}

						// Kind 3
						handleSequence((SimpleNode) n.jjtGetChild(2), cbBlock);
					}
					root.addChild(cnl);
					log.info(cnl);
				}
			}
		}
		if (s.jjtGetNumChildren() > 1)
		{
			log.info("sequence has more than 1 child??!");
		}
		return this;
	}
}
