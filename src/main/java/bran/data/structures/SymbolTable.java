package bran.data.structures;

import java.util.Collection;
import java.util.HashMap;

import bran.compiler.LogMessages;

public class SymbolTable
{
	HashMap<String, Definition> table = new HashMap<>();

	public SymbolTable()
	{

	}

	public int getSize()
	{
		return table.size();
	}

	public Collection<Definition> getSymbols()
	{
		return table.values();
	}

	public boolean contains(Definition def)
	{
		return contains(def.name);
	}

	public boolean contains(String name)
	{
		return table.containsKey(name);
	}

	public void add(Definition def)
	{
		if (table.containsKey(def.name))
		{
			throw new IllegalStateException("Variable " + def.name + " already exists in SymbolTable. Defined at line "
					+ table.get(def.name).getStartline());
		} else
		{
			table.put(def.name, def);
		}
	}

	public Boolean check(String image)
	{
		return table.containsKey(image);
	}

	public Definition get(String image)
	{
		if (check(image))
			return table.get(image);
		else
			return null;
	}
}
