package bran.data.structures;

import java.util.ArrayList;

import bran.compiler.Main;
import bran.parser.BRAN_AST_ArrayDeclaration;
import bran.parser.BRAN_AST_ParameterDeclaration;
import bran.parser.BRAN_AST_TypeSpecifier;
import bran.parser.SimpleNode;
import bran.parser.Token;
public class Definition
{
	String					name;
	DataType				type;
	int						startline		= -1;
	int						varCounter		= -1;
	StructDef				struct			= null;
	ArrayList<Definition>	fields;
	private int				numPointers		= 0;
	private boolean			isAllocated		= false;
	private boolean			isArray			= false;
	private boolean			isStruct		= false;
	private int				varScopeCounter	= -1;

	// TODO: array? pointer?
	public Definition(final String name, final String type) {
		this(name, DataType.valueOf(type.toUpperCase()));
	}

	public Definition(final String name, final String type, final int line) {
		this(name, type);
		setStartLine(line);
	}

	public Definition(final String name, final DataType type, final int line) {
		this(name, type);
		setStartLine(line);
	}

	public Definition(final String name, final DataType type) {
		this.name = name;
		this.type = type;
		varCounter = Main.getDefinitionCounter(this);
	}

	public Definition(final BRAN_AST_ParameterDeclaration param) {
		name = param.jjtGetValue().toString();
		if(name.equals("="))
		{
			System.out.println("oO");
		}
		if ((param.jjtGetChild(0) instanceof BRAN_AST_ArrayDeclaration)) {
			System.out.println(((SimpleNode) param.jjtGetChild(0).jjtGetChild(0)).jjtGetValue());
			type = DataType
					.valueOf(((SimpleNode) param.jjtGetChild(0).jjtGetChild(0)).jjtGetValue().toString().toUpperCase());
			isArray = true;
		} else if ((param.jjtGetChild(0)) instanceof BRAN_AST_TypeSpecifier
				&& ((SimpleNode) param.jjtGetChild(0)).jjtGetValue().toString().equals("struct"))
		{
			type = DataType.STRUCT;
			struct = Main.getStruct(((SimpleNode) param.jjtGetChild(0).jjtGetChild(0)).jjtGetValue().toString());
			loadStructFields(((SimpleNode) param.jjtGetChild(0).jjtGetChild(0)).jjtGetValue().toString());
		}
		else {
			type = DataType.valueOf(((SimpleNode) param.jjtGetChild(0)).jjtGetValue().toString().toUpperCase());
		}
		varCounter = Main.getDefinitionCounter(this);
		startline = ((Token) param.jjtGetValue()).beginLine;
	}

	public void setPointers(final int amount) {
		numPointers = amount;
	}

	public int getPointers() {
		return numPointers;
	}

	public void loadStructFields(final String structName) {
		if ((type == DataType.STRUCT) && (fields == null)) {
			// if fields not null they must already have been loaded
			struct = Main.getStruct(structName);
			fields = new ArrayList<>(struct.getFields());
		}
	}

	public String getName() {
		return name;
	}

	public DataType getType() {
		return type;
	}

	public String getDumpIncludingStruct() {
		if (type == DataType.STRUCT) {
			return struct.getCompleteDumpString();
		} else {
			return toString();
		}
	}

	@Override
	public String toString() {
		return name + (startline >= 0 ? ":" + startline : "") + ":" + type.name() + (type == DataType.STRUCT
				? ":defined-as:" + struct.getDefinition().getName() + ":at:" + struct.getDefinition().getStartline()
				: "") + ":pointers:" + getPointers();
	}

	public void setStartLine(final int line) {
		startline = line;
	}

	public int getStartline() {
		return startline;
	}

	public void setStruct(final StructDef structDef) {
		struct = structDef;
	}

	public StructDef getStructDef() {
		return struct;
	}

	public boolean isAllocated() {
		return isAllocated;
	}

	public void setAllocated(final boolean val) {
		isAllocated = val;
	}

	public void setArray(final boolean type) {
		isArray = type;
	}

	public boolean isArray() {
		return isArray;
	}
	
	public void setStruct(final boolean type)
	{
		isStruct = type;
	}
	
	public boolean isStruct()
	{
		return isStruct;
	}

	public int getVarScopeCounter() {
		return varScopeCounter;
	}

	public Definition setVarScopeCounter(int varScopeCounter) {
		this.varScopeCounter = varScopeCounter;
		return this;
	}
	
	public boolean structContainsField(String fieldIdent){
		if (isStruct){			
			for(Definition tmp : fields){
				if(tmp.getName().equals(fieldIdent)){
					return true;
				}
			}
		}
		return false;
	}
}
