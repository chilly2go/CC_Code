package bran.data.structures;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bran.parser.BRAN_AST_Identifier;
import bran.parser.BRAN_AST_Pointer;
import bran.parser.BRAN_AST_StructDeclaration;
import bran.parser.BRAN_AST_StructDeclarationList;
import bran.parser.BRAN_AST_StructDefinition;
import bran.parser.BRAN_AST_TypeSpecifier;
import bran.parser.SimpleNode;
import bran.parser.Token;

public class StructDef
{
	Definition					self;
//	private SymbolTable			fields	= new SymbolTable();
	private SymbolTable			fields;
	private static final Logger	log		= LogManager.getLogger();

	public String getName()
	{
		return self.getName();
	}

	public Definition getDefinition()
	{
		return self;
	}

	public Boolean contains(String field)
	{
		return fields.contains(field);
	}

	public StructDef(BRAN_AST_StructDefinition s)
	{
		
		//Handelt es sich um einen Struct-Header oder ein Struct mit Body (auch leer m�glich)
		if (s.jjtGetNumChildren() >= 1) {
			
			Token ident = (Token) ((SimpleNode) s.jjtGetChild(0)).jjtGetValue();
			self = new Definition(ident.image, ((Token) s.jjtGetValue()).image, ident.beginLine);
			
			//Handelt es sich um eine volle Definition mit Body?
			if (s.jjtGetNumChildren() == 2){
				
				addFields((BRAN_AST_StructDeclarationList) s.jjtGetChild(1));
			}
		} else {
			
			throw new IllegalArgumentException("Invalid Struct at line " + ((Token) s.jjtGetValue()).beginLine);
		}
	}

	private void addFields(BRAN_AST_StructDeclarationList node)
	{
		fields = new SymbolTable();
		
		for (int i = 0; i < node.jjtGetNumChildren(); i++)
		{
			SimpleNode child = (SimpleNode) node.jjtGetChild(i);
			// for each child add definition
			if (child instanceof BRAN_AST_StructDeclaration)
			{
				// valid field
				Token ident = (Token) ((SimpleNode) child.jjtGetChild(1)).jjtGetValue();
				BRAN_AST_TypeSpecifier type = (BRAN_AST_TypeSpecifier) child.jjtGetChild(0);
				Definition def = new Definition(ident.image, ((Token) type.jjtGetValue()).image, ident.beginLine);
				if (type.jjtGetNumChildren() >= 1)
				{
					int pointerSearchStartChild = 0;
					if (type.jjtGetChild(0) instanceof BRAN_AST_Identifier && def.getType() == DataType.STRUCT)
					{
						// its a struct
						def.loadStructFields(((Token) ((BRAN_AST_Identifier) type.jjtGetChild(0)).jjtGetValue()).image);
						pointerSearchStartChild++;
					}
					int p_count = 0;
					for (int x = pointerSearchStartChild; x < type.jjtGetNumChildren(); x++)
					{
						if (type.jjtGetChild(x) instanceof BRAN_AST_Pointer)
						{
							p_count++;
						} else
						{
							SimpleNode n = (SimpleNode) type.jjtGetChild(x);
							Token t = (Token) n.jjtGetValue();
							log.error("Unexpected node {} line {}", t.image, t.beginLine);
							break;
						}
					}
					def.setPointers(p_count);
				}
				fields.add(def);
			} else
			{
				// invalid field
				throw new IllegalArgumentException("Struct containing invalid field definition at line "
						+ ((Token) child.jjtGetValue()).beginLine);
			}
		}
	}
	
	public boolean isDefinitionComplete()
	{
		return fields != null;
	}
	
	public void addBlock(BRAN_AST_StructDeclarationList node){
		
		addFields(node);
	}

	public Collection<Definition> getFields()
	{
		return fields.getSymbols();
	}
	
	public Definition getField(String name)
	{
		if (fields.contains(name))
		{
			return fields.get(name);
		}
		return null;
	}

	public String getDumpString()
	{
		return self.toString();
	}

	public String getCompleteDumpString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Struct: ").append(self).append("\n");
		for (Definition definition : fields.getSymbols())
		{
			sb.append("\tFields: ").append(definition).append("\n");
		}
		return sb.toString();
	}

	public void dump()
	{
		log.debug("Struct: {}", self);		
		if (isDefinitionComplete()) {

			for (Definition definition : fields.getSymbols()) {
				
				log.debug("\tFields: {}", definition);
			}
		}	
	}

	public void updateSelfDefinition()
	{
		self.setStruct(this);
	}
}
