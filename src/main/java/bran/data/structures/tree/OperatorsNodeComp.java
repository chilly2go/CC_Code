package bran.data.structures.tree;

import java.util.ArrayList;
import java.util.HashMap;

import bran.parser.SimpleNode;

public class OperatorsNodeComp extends OperatorsNode
{
	HashMap<String, String> compOperators = new HashMap<>();

	public OperatorsNodeComp(SimpleNode n)
	{
		super(n);
		
		this.restriction = new NodeRestrictions();
		this.restriction.addAllowedTypes(NodeTypes.INT).addAllowedTypes(NodeTypes.CALC);
		compOperators.put("==", "eq");
		compOperators.put("!=", "neq");
		compOperators.put("<", "le");
		compOperators.put(">", "gr");
		compOperators.put(">=", "geq");
		compOperators.put("<=", "leq");// TODO: check - currently missing in notes
	}

	@Override
	public NodeTypes getReturnType()
	{
		return NodeTypes.BOOLEAN;
	}

	@Override
	public boolean check()
	{
		this.checked = true;
		if (this.restriction.has(getNumChilds()))
		{
			if (getChildAt(0).getReturnType() == getChildAt(1).getReturnType())
			{
				return isChecked();
			}
		}
		return false;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();//super.getCode();
		code.addAll(getChildAt(0).getCode());
		code.addAll(getChildAt(1).getCode());
		code.add(compOperators.get(getName()));
		return code;
	}

}
