package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;
import bran.parser.Token;

public abstract class TreeNode
{
	protected Boolean				checked		= false;
	protected ArrayList<TreeNode>	children	= new ArrayList<>(3);
	private int						col			= 0;
	private int						line		= 0;
	private String					name;
	private TreeNode				parent		= null;
	protected NodeRestrictions		restriction;
	protected NodeTypes				returntype	= NodeTypes.NON;
	protected NodeTypes				type		= NodeTypes.NON;
	ControlNodeFunction				rootNode;
	
	protected String label = "";
	char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	int jump = 0;

	public TreeNode(SimpleNode n)
	{
		if (n.jjtGetValue() instanceof Token)
		{
			Token t = (Token) n.jjtGetValue();
			name = t.image;
			line = t.beginLine;
			col = t.beginColumn;
		} else if (n.jjtGetValue() != null)
		{
			name = (String) n.jjtGetValue();
		} else
		{
			name = n.getClass().getSimpleName();
		}
		this.restriction = new NodeRestrictions();
	}

	public TreeNode addChild(TreeNode node)
	{
		children.add(node);
		return this;
	}

	public abstract boolean check();

	public TreeNode getChildAt(int i)
	{
		if (i >= 0 && i < getNumChilds())
		{
			return this.children.get(i);
		} else
		{
			throw new IndexOutOfBoundsException(
					"Specified index " + i + " for node " + getName() + " having " + getNumChilds() + " children");
		}
	}

	public ArrayList<String> getCode() throws IllegalAccessException
	{
		return getCode(false);
	}

	public abstract ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException;

	public int getCol()
	{
		return col;
	}

	public int getLine()
	{
		return line;
	}

	public String getName()
	{
		return name;
	}

	public int getNumChilds()
	{
		return this.children.size();
	}

	public TreeNode getParent()
	{
		return parent;
	}

	public NodeTypes getReturnType()
	{
		return returntype;
	}

	public NodeTypes getType()
	{
		return type;
	}

	public boolean isChecked()
	{
		return checked;
	}

	public TreeNode setCol(int col)
	{
		this.col = col;
		return this;
	}

	public TreeNode setLine(int line)
	{
		this.line = line;
		return this;
	}

	public TreeNode setName(String name)
	{
		this.name = name;
		return this;
	}

	public TreeNode setParent(TreeNode parent)
	{
		this.parent = parent;
		return this;
	}

	public String toPrint()
	{
		return name + " (" + line + "/" + col + ")";
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String l) {
		label = l;
	}
	
	public int getJump() {
		return jump;
	}
	
	public void setJump(int j) {
		jump = j;
	}
}
