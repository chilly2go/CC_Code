package bran.data.structures.tree;

import java.util.ArrayList;

import javax.management.InvalidAttributeValueException;

import bran.data.structures.DataType;
import bran.data.structures.Definition;
import bran.parser.SimpleNode;

public class VarsNodeStruct extends VarsNode
{
	String fieldname;

	public VarsNodeStruct(SimpleNode n, Definition def)
	{
		super(n, def);
		if (def.getType() != DataType.STRUCT)
			throw new IllegalArgumentException("Created a StructNode without Struct " + this.toPrint());
	}

	// this.restriction.setRange(1).addAllowedTypes(NodeTypes.INT);
	public VarsNodeStruct setField(String field) throws InvalidAttributeValueException
	{
		if (self.getStructDef().contains(field))
		{
			this.fieldname = field;
			// TODO: calc offset
		} else
			throw new InvalidAttributeValueException(
					"Struct (" + this.toPrint() + ") does not contain a field named " + field);
		return this;
	}

	@Override
	@Deprecated
	public Object getValue()
	{
		return null;
	}

	@Override
	public ArrayList<String> getCode()
	{
		ArrayList<String> code = new ArrayList<>();
		code.add("loada"); // TODO: load struct (field?)
		return code;
	}

	@Override
	public NodeTypes getReturnType()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean check()
	{
		// TODO Auto-generated method stub
		return false;
	}
}
