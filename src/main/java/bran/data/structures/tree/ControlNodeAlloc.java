package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class ControlNodeAlloc extends ControlNode
{

	// private VarsNode allocType;
	private int allocSize = 0;

	/**
	 * type + size;
	 * 
	 * @param n
	 */
	public ControlNodeAlloc(SimpleNode n)
	{
		super(n);
		this.restriction.setRange(1);
		this.restriction.addAllowedTypes(NodeTypes.STRUCT).addAllowedTypes(NodeTypes.ARRAY);
	}

	@Override
	public boolean check()
	{
		if (this.restriction.has(getNumChilds()))
		{
			if (this.restriction.contains(getChildAt(0).getReturnType()))
			{
				this.checked = true;
			}
		}
		return isChecked();
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		VarsNode v = (VarsNode) getChildAt(0);
		if (this.allocSize > 0)
		{
			code.add("loadc " + this.allocSize);
			code.add(v.codeR());
			code.add("load");
			code.add("mul");
			code.add("new");
			//code.add("storer ".concat(v.getPofV() + ""));
		}
		return code;
	}

	public int getAllocSize()
	{
		return allocSize;
	}

	public void setAllocSize(int allocSize)
	{
		this.allocSize = allocSize;
	}

}
