package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class ControlNodeConditionalIf extends ControlNode
{

	/*
	 * children = condition / then / else
	 */
	TreeNode	thenpart;	 // have to return var or constant
	TreeNode	elsepart;	 // have to return var or constant

	public ControlNodeConditionalIf setThen(TreeNode n)
	{
		thenpart = n;
		return this;
	}

	public ControlNodeConditionalIf(SimpleNode n)
	{
		super(n);
		this.restriction.setRange(1).setAllowedTypes(NodeTypes.BOOLEAN);
	}

	@Override
	public boolean check()
	{
		this.checked = true;
		if (this.restriction.has(getNumChilds()))
		{
			if (getChildAt(0).getReturnType() == getChildAt(1).getReturnType())
			{
				return isChecked();
			}
		}
		return false;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		
		// Condition
		TreeNode l = getChildAt(0);
		
		if(!label.isEmpty()) {
			code.add(new String(label).concat(": ").concat(l.getCode().get(0)));
			for(int i = 1; i < l.getCode().size(); i++) {
				code.add(l.getCode().get(i));
			}
		}
		else {
			code.addAll(l.getCode());
		}

		if(this.getNumChilds() == 3) {
			code.add("jumpz " + String.valueOf(alphabet[jump-1])); // jumpz B
		}
		else {
			code.add("jumpz " + String.valueOf(alphabet[jump])); // jumpz C
		}
		
		// If-Block A
		TreeNode m = getChildAt(1);
		code.addAll(m.getCode());
		if(this.getNumChilds() == 3) {
			code.add("jumpz " + String.valueOf(alphabet[jump])); // jumpz C
		}
		
		// Else-Block B
		if(this.getNumChilds() == 3) {
			TreeNode r = getChildAt(2);
			code.addAll(r.getCode());
		}
		
		// danach C
		return code;
	}
}
