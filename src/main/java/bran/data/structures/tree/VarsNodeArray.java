package bran.data.structures.tree;

import java.util.ArrayList;

import bran.data.structures.DataType;
import bran.data.structures.Definition;
import bran.parser.SimpleNode;

public class VarsNodeArray extends VarsNode
{
	int size = 1;

	public VarsNodeArray(SimpleNode n, Definition def)
	{
		super(n, def);
		// if (def.getPointers() <= 0)
		// throw new IllegalArgumentException("Provided Definition (" + this.toPrint()
		// + ") has 0 Pointers and does not qualify for beeing an Array");
		children = new ArrayList<>();
		this.restriction.setRange(1);
	}

	@Override
	public NodeTypes getReturnType()
	{
		if (type == null)
		{
			DataType t = self.getType();
			if (t == DataType.INT)
			{
				type = NodeTypes.INT;
			}
			if (t == DataType.BOOL)
			{
				type = NodeTypes.BOOLEAN;
			}
			if (t == DataType.STRUCT)
			{
				type = NodeTypes.STRUCT;
			}
			this.restriction.setAllowedTypes(type);
		}
		return type;
	}

	@Override
	public Object getValue()
	{
		// TODO Auto-generated method stub
		return this.value;
	}

	@Override
	public boolean check()
	{
		// TODO Auto-generated method stub
		return false;
	}

	public ArrayList<String> getCode(boolean lval) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		// if (lval)
		// {
		// code.add(codeL());
		// code.add("load");
		// } else
		// {
		code.add(this.codeL());
		code.add("load");
		code.addAll(this.getChildAt(0).getCode());
		getReturnType();
		code.add("loadc " + this.size);
		code.add("mul");
		code.add("add");
		code.add("load");
		// }
		return code;
	}

}
