package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class VarsNodeConstant extends VarsNode
{

	public VarsNodeConstant(SimpleNode n, int val)
	{
		super(n, null);
		this.value = val;
	}

	@Override
	public Integer getValue()
	{
		return (Integer) this.value;
	}

	@Override
	public boolean check()
	{
		return isChecked();
	}

	public String codeR()
	{
		return "loadc " + this.value;
	}
	public ArrayList<String> getCode(boolean lval) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		if (lval)
			code.add(codeL());
		else
		{
			code.add(codeR());
		}
		return code;
	}
}
