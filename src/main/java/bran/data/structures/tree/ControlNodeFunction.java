package bran.data.structures.tree;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bran.compiler.FunctionHandler;
import bran.compiler.Main;
import bran.data.structures.SymbolTable;
import bran.exceptions.AllocSizeException;
import bran.parser.SimpleNode;

// children = body
public class ControlNodeFunction extends ControlNode
{
	NodeTypes					returntype;
	ArrayList<VarsNode>			params;
	int							ctr			= 0;
	int							k			= 0;
	SymbolTable					st;
	int							alloc_size	= 0;
	private static final Logger	log			= LogManager.getLogger();

	public ControlNodeFunction(SimpleNode n)
	{
		super(n);
		FunctionHandler self = Main.getFunctionHandler(getName());
		this.returntype = NodeTypes.valueOf(self.getReturnType().name());
		params = new ArrayList<>(self.getParamSize() + 1);
		rootNode = this;
		ctr -= 2;
	}

	public ControlNodeFunction setSymbolTable(SymbolTable st)
	{
		this.st = st;
		this.k = this.st.getSize() - params.size();
		if (k < 0) throw new AllocSizeException("More params than symbols at function " + this.getName());
		return this;
	}

	public SymbolTable getSymbolTable()
	{
		return this.st;
	}

	public ControlNodeFunction addParam(VarsNode param)
	{
		params.add(param);
		param.rootNode = this;
		param.setPofV(--ctr);
		return this;
	}

	@Override
	public ArrayList<String> getCode(boolean lval) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		code.add("");
		code.add(new String("_").concat(getName()).concat(": ").concat("enter ").concat("{{q}}"));
		if (k > 0) code.add("alloc " + k);
		for (TreeNode child : children)
		{
			if (Main.Debug)
			{
				log.info("Gen for {}|Line: {}", child.getClass().getName(), child.getLine());
				code.add(child.getClass().getName() + "|" + child.getLine());
			}
			code.addAll(child.getCode());
		}
		code.add("return");
		// replacing placeholders
		boolean found = false;
		for (int i = 0; i < code.size() && !found; i++)
		{
			if (code.get(i).contains("{{q}}"))
			{
				code.set(i, code.get(i).replace("{{q}}", new Integer((code.size())).toString()));
				found = true;
			}
		}
		return code;
	}

	public int getParamSize()
	{
		return params.size();
	}

	@Override
	public NodeTypes getReturnType()
	{
		return returntype;
	}

	@Override
	public boolean check()
	{
		return false;
	}

}
