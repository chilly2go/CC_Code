package bran.data.structures.tree;

import java.util.ArrayList;

import com.google.common.collect.Range;

import bran.compiler.Main;

public class NodeRestrictions
{
	Range<Integer>			numChilds;
	ArrayList<NodeTypes>	allowedTypes;

	public NodeRestrictions()
	{
		this(null);
	}

	public NodeRestrictions(Range<Integer> numChilds)
	{
		this(numChilds, null);
	}

	public NodeRestrictions(Range<Integer> numChilds, ArrayList<NodeTypes> types)
	{
		this.numChilds = numChilds;
		this.allowedTypes = types;
		if (this.numChilds == null) setRange(Main.DefaultNodeChildNum);
	}

	public NodeRestrictions setRange(int num)
	{
		return setRange(num, num);
	}

	public NodeRestrictions setRange(int from, int to)
	{
		numChilds = Range.closed(from, to);
		return this;
	}

	public NodeRestrictions setAllowedTypes(ArrayList<NodeTypes> types)
	{
		this.allowedTypes = types;
		return this;
	}

	public NodeRestrictions addAllowedTypes(ArrayList<NodeTypes> type)
	{
		for (NodeTypes nodeTypes : type)
		{
			addAllowedTypes(nodeTypes);
		}
		return this;
	}

	public NodeRestrictions addAllowedTypes(NodeTypes type)
	{
		if (this.allowedTypes == null)
		{
			this.allowedTypes = new ArrayList<>();
		}
		this.allowedTypes.add(type);
		return this;
	}

	public NodeRestrictions setAllowedTypes(NodeTypes type)
	{
		ArrayList<NodeTypes> l = new ArrayList<>();
		l.add(type);
		addAllowedTypes(l);
		return this;
	}

	/**
	 * checks if num is maximum of range
	 * 
	 * @param num
	 * @return
	 */
	public boolean has(int num)
	{
		return numChilds.upperEndpoint() == num;
	}

	public boolean contains(NodeTypes type)
	{
		return allowedTypes.contains(type);
	}

	public boolean contains(int num)
	{
		return numChilds.contains(num);
	}
}
