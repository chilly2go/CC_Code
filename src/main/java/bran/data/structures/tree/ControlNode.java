package bran.data.structures.tree;

import bran.parser.SimpleNode;

public abstract class ControlNode extends TreeNode
{
	public ControlNode(SimpleNode n)
	{
		super(n);
		this.restriction = new NodeRestrictions();
		this.restriction.setRange(1, Integer.MAX_VALUE);
	}

}
