package bran.data.structures.tree;

import java.util.ArrayList;

import bran.data.structures.Definition;
import bran.parser.SimpleNode;

// TODO: add proper child classes for var types
public abstract class VarsNode extends TreeNode
{
	protected Definition	self;
	protected Object		value;
	protected int			p_of_v	= 0;

	/**
	 * 
	 * @param n
	 * @param def
	 *            Definition of var or null for constant value
	 */
	public VarsNode(SimpleNode n, Definition def)
	{
		super(n);
		this.self = def;
		children = null;
		this.restriction = new NodeRestrictions();
		this.restriction.setRange(0);
		if (isVar())
		{
			this.p_of_v = self.getVarScopeCounter();
			if (this.p_of_v < 0) this.p_of_v -= 2;
		}
	}

	public int getPofV()
	{
		return p_of_v;
	}

	public String codeL()
	{
		return "loadrc " + p_of_v;
	}

	public String codeR()
	{
		return "loadrc " + p_of_v;
	}

	public void setPofV(int pv)
	{
		p_of_v = pv;
	}

	public ArrayList<String> getCode(boolean lval) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		if (lval)
			code.add(codeL());
		else
		{
			code.add(codeR());
			code.add("load");
		}
		return code;
	}

	public boolean isVar()
	{
		return self != null;
	}

	public boolean isConstant()
	{
		return self == null;
	}

	public abstract Object getValue();

	public int getSize()
	{
		return 1;
	}
}
