package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class OperatorsNodeNot extends OperatorsNode
{

	public OperatorsNodeNot(SimpleNode n)
	{
		super(n);
		this.restriction = new NodeRestrictions();
		this.restriction.setRange(1).addAllowedTypes(NodeTypes.BOOLEAN);
		returntype = NodeTypes.BOOLEAN;
		type = NodeTypes.NOT;
	}

	@Override
	public boolean check()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		// TODO Auto-generated method stub
		return null;
	}

}
