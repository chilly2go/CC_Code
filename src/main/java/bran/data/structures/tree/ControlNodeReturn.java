package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class ControlNodeReturn extends ControlNode
{

	public ControlNodeReturn(SimpleNode n)
	{
		super(n);
		this.restriction.setRange(1);
	}

	@Override
	public boolean check()
	{
		return true;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		if(!label.isEmpty()) {
			code.add(new String(label).concat(": ").concat(getChildAt(0).getCode().get(0)));
			for(int i = 1; i < getChildAt(0).getCode().size(); i++) {
				code.add(getChildAt(0).getCode().get(i));
			}
		}
		else {
			code.addAll(getChildAt(0).getCode());
		}
		code.add("storer -3");
		return code;
	}

}
