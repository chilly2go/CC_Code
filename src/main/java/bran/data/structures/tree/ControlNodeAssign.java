package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class ControlNodeAssign extends ControlNode
{

	public ControlNodeAssign(SimpleNode n)
	{
		super(n);
		this.restriction.setRange(2, Integer.MAX_VALUE);
	}

	@Override
	public boolean check()
	{
		this.checked = true;
		if (this.restriction.has(getNumChilds()))
		{
			if (getChildAt(0).getReturnType() == getChildAt(1).getReturnType())
			{
				return isChecked();
			}
		}
		return false;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		TreeNode l = getChildAt(0);
		TreeNode r = getChildAt(1);
		code.addAll(r.getCode());
		if (l instanceof VarsNode)
		{
			code.add("storer " + ((VarsNode) l).getPofV());
			code.add("pop");
		} else
		{
			code.addAll(l.getCode(true));
			code.add("store");
		}
		return code;
	}

}
