package bran.data.structures.tree;

import java.util.ArrayList;

import bran.compiler.FunctionHandler;
import bran.compiler.Main;
import bran.data.structures.DataType;
import bran.parser.SimpleNode;

public class ControlNodeFunctionCall extends ControlNode
{

	public ControlNodeFunctionCall(SimpleNode n)
	{
		super(n);
		FunctionHandler func = Main.getFunctionHandler(getName());
		DataType type = func.getReturnType();
		this.returntype = NodeTypes.valueOf(type.name());
	}

	@Override
	public boolean check()
	{
		return false;
	}

	public void testi()
	{
		ArrayList<String> code = new ArrayList<>();
		for (TreeNode child : children)
		{
			if (child instanceof ControlNodeFunction)
			{
				if (((ControlNodeFunction) child).getParamSize() == 0)
					code.add("alloc 1");
				code.add("mark");
				code.add("loadc _" + child.getName());
				code.add("call");
				code.add("slide {{s}}");
			}
		}
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		if (getNumChilds() == 0)
		{
			// no param? we reserve a cell for return val
			if (this.getReturnType() != NodeTypes.VOID)
				code.add("alloc 1");
		}
		for (int i = getNumChilds() - 1; i >= 0; i--)
		{
			code.addAll(getChildAt(i).getCode());
		}
		code.add("mark");
		code.add("loadc _" + this.getName());
		code.add("call");
		code.add("slide " + checkSlide());
		return code;
	}
	public int checkSlide()
	{
		int s=0;
		s = getNumChilds();
		if (this.getReturnType() != NodeTypes.VOID)
			s--;
		if(s < 0) s=0;
		return s;
	}
}
