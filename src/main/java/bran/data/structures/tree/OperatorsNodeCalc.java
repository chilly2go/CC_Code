package bran.data.structures.tree;

import java.util.ArrayList;
import java.util.HashMap;

import bran.parser.SimpleNode;

public class OperatorsNodeCalc extends OperatorsNode
{
	HashMap<String, String> calcOperators = new HashMap<>();

	public OperatorsNodeCalc(SimpleNode n)
	{
		super(n);
		this.restriction = new NodeRestrictions();
		this.restriction.addAllowedTypes(NodeTypes.INT).addAllowedTypes(NodeTypes.CALC);
		calcOperators.put("+", "add");
		calcOperators.put("-", "sub");
		calcOperators.put("*", "mul");
		calcOperators.put("/", "div");
		calcOperators.put("%", "mod");
		calcOperators.put("++", "add");
		calcOperators.put("--", "sub");
	}

	public boolean check()
	{
		// TODO: eval resitrctions
		return false;
	}

	@Override
	public NodeTypes getReturnType()
	{
		return NodeTypes.INT;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		code.addAll(getChildAt(0).getCode());
		code.addAll(getChildAt(1).getCode());
		code.add(calcOperators.get(getName()));
		return code;
	}

}
