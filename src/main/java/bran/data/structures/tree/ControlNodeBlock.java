package bran.data.structures.tree;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;

import bran.parser.SimpleNode;

public class ControlNodeBlock extends ControlNode{

	String index = "";
	
	public ControlNodeBlock(SimpleNode n) {
		super(n);
		this.restriction.setRange(2, Integer.MAX_VALUE);
	}

	@Override
	public boolean check() {
		this.checked = true;
		if (this.restriction.has(getNumChilds()))
		{
			if (getChildAt(0).getReturnType() == getChildAt(1).getReturnType())
			{
				return isChecked();
			}
		}
		return false;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException {
		ArrayList<String> code = new ArrayList<>();
		TreeNode l = getChildAt(0);

		if(l.getLabel().isEmpty()) {
			code.add(new String(label).concat(": ").concat(l.getCode().get(0)));
			for(int i = 1; i < l.getCode().size(); i++) {
				code.add(l.getCode().get(i));
			}
		}
		else {
			code.addAll(l.getCode());
		}

		for(int i = 1; i < this.getNumChilds(); i++) {
			TreeNode r = getChildAt(i);
			code.addAll(r.getCode());
		}
		return code;
	}

	
	public void setIndex(String idx) {
		index = idx;
	}
	
	public String getIndex() {
		return index;
	}
}
