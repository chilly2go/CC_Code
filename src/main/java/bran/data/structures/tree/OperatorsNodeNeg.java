package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class OperatorsNodeNeg extends OperatorsNode
{

	public OperatorsNodeNeg(SimpleNode n)
	{
		super(n);
		this.restriction = new NodeRestrictions();
		this.restriction.setRange(1).addAllowedTypes(NodeTypes.INT);
	}

	@Override
	public NodeTypes getReturnType()
	{
		return NodeTypes.INT;
	}

	@Override
	public boolean check()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		// TODO Auto-generated method stub
		return null;
	}

}
