package bran.data.structures.tree;

public enum NodeTypes
{// TODO: add missing types
	CONSTANT,
	INT,
	VOID,
	BOOLEAN,
	STRUCT,
	ARRAY,
	CALC,
	COMPARE,
	NEGATE,
	NOT,
	FUNCTION,
	RETURN,
	IF_STATEMENT,
	WHILE_LOOP,
	FOR_LOOP,
	NON // for non existing types like TreeNode
}
