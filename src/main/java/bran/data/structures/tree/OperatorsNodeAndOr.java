package bran.data.structures.tree;

import java.util.ArrayList;
import java.util.HashMap;

import bran.parser.SimpleNode;

public class OperatorsNodeAndOr extends OperatorsNode {

	HashMap<String, String> compOperators = new HashMap<>();

	public OperatorsNodeAndOr(SimpleNode n) {
		super(n);

		this.restriction = new NodeRestrictions();
		this.restriction.addAllowedTypes(NodeTypes.INT).addAllowedTypes(NodeTypes.CALC);
		compOperators.put("&&", "and");
		compOperators.put("||", "or");
		compOperators.put("^", "xor");
	}
	
	@Override
	public NodeTypes getReturnType()
	{
		return NodeTypes.BOOLEAN;
	}

	@Override
	public boolean check() {
		this.checked = true;
		if (this.restriction.has(getNumChilds()))
		{
			if (getChildAt(0).getReturnType() == getChildAt(1).getReturnType())
			{
				return isChecked();
			}
		}
		return false;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException {
		ArrayList<String> code = new ArrayList<>();//super.getCode();
		code.addAll(getChildAt(0).getCode());
		code.addAll(getChildAt(1).getCode());
		code.add(compOperators.get(getName()));
		return code;
	}

}
