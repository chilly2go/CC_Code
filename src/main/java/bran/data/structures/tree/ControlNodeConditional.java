package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class ControlNodeConditional extends ControlNode
{
	private ArrayList<TreeNode> elseblock;
	private TreeNode condition = null;
	private String jumpsymbol = "";

	/*
	 * children = then
	 */
	public ControlNodeConditional(SimpleNode n)
	{
		super(n);
	}

	@Override
	public boolean check()
	{
		return false;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();
		code.addAll(this.condition.getCode());
		code.add("jumpz "+this.jumpsymbol+"1");
		for (TreeNode child : children)
		{
			code.addAll(child.getCode());
		}
		if(this.getElseSize()>0)
		{
			if(code.get(code.size()-1).equals("pop"))
				code.remove(code.size()-1);
			code.add("jump "+this.jumpsymbol+"2");
			int mark = code.size();
			for(int i = 0; i < this.getElseSize();i++)
			{
				code.addAll(this.getElseblock().get(i).getCode());
			}
			code.add(this.jumpsymbol+"2: loadc 1");
			code.add("pop");
			code.set(mark, this.jumpsymbol+"1: ".concat(code.get(mark)));
		}else
		{
			code.add(this.jumpsymbol+"1: loadc 1");
			code.add("pop");
		}
		return code;
	}

	public ArrayList<TreeNode> getElseblock()
	{
		return elseblock;
	}

	public int getElseSize()
	{
		return (this.elseblock == null ? 0 : this.elseblock.size());
	}

	public ControlNodeConditional addElseblock(TreeNode elseblock)
	{
		if (this.elseblock == null) this.elseblock = new ArrayList<>();
		this.elseblock.add(elseblock);
		return this;
	}

	public TreeNode getCondition()
	{
		return condition;
	}

	public void setCondition(TreeNode condition)
	{
		this.condition = condition;
	}

	public String getJumpsymbol()
	{
		return jumpsymbol;
	}

	public void setJumpsymbol(String jumpsymbol)
	{
		this.jumpsymbol = jumpsymbol.concat("_if_");
	}

}
