package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class ControlNodeLoop extends ControlNode
{
/*
 * init -> child (only for)
 * condition -> child
 * block -> children
 * step -> child (only for)
 */
	public ControlNodeLoop(SimpleNode n)
	{
		super(n);
		this.restriction.setRange(2, Integer.MAX_VALUE);
	}

	@Override
	public boolean check()
	{
		if (this.restriction.contains(this.children.size()))
		{
			if (this.restriction.contains(this.children.get(0).getReturnType())) this.checked = true;
		}
		return this.checked;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		ArrayList<String> code = new ArrayList<>();

		TreeNode l = getChildAt(0);
		TreeNode r = getChildAt(1);
		
		if(!label.isEmpty()) {
			code.add(new String(label).concat(": ").concat(l.getCode().get(0)));
			for(int i = 1; i < l.getCode().size(); i++) {
				code.add(l.getCode().get(i));
			}
		}
		else {
			code.addAll(l.getCode());
		}
		
		code.add("jumpz " + String.valueOf(alphabet[jump]));
		code.addAll(r.getCode());
		code.add("jump " + String.valueOf(alphabet[jump-2]));
		return code;
	}

}
