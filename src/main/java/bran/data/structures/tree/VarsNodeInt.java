package bran.data.structures.tree;

import java.util.ArrayList;

import bran.data.structures.Definition;
import bran.parser.SimpleNode;

public class VarsNodeInt extends VarsNode
{

	public VarsNodeInt(SimpleNode n, Definition def)
	{
		super(n, def);
		type = NodeTypes.INT;
		returntype = type;
	}

	public VarsNodeInt setValue(Integer i)
	{
		this.value = i;
		return this;
	}

	@Override
	public Integer getValue()
	{
		return (Integer) this.value;
	}

	@Override
	public ArrayList<String> getCode()// probably need to specify if L- or R-value
	{
		ArrayList<String> code = new ArrayList<>();
		code.add("loadrc " + this.p_of_v); // TODO: load int address -> probably "loadra"
		code.add("load");
		return code;
	}

	@Override
	public NodeTypes getReturnType()
	{
		return NodeTypes.INT;
	}

	@Override
	public boolean check()
	{
		// TODO Auto-generated method stub
		return false;
	}
}
