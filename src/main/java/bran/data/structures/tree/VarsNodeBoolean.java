package bran.data.structures.tree;

import java.util.ArrayList;

import bran.data.structures.Definition;
import bran.parser.SimpleNode;

public class VarsNodeBoolean extends VarsNode
{

	public VarsNodeBoolean(SimpleNode n, Definition def)
	{
		super(n, def);
		type = NodeTypes.BOOLEAN;
		returntype = type;
	}

	public VarsNodeBoolean setValue(Boolean b)
	{
		this.value = b;
		return this;
	}

	@Override
	public Boolean getValue()
	{
		return (Boolean) this.value;
	}

	@Override
	public ArrayList<String> getCode()// probably need to specify if L- or R-value
	{
		ArrayList<String> code = new ArrayList<>();
		code.add("loada"); // TODO: load bool address -> probably "loadra"
		return code;
	}

	@Override
	public NodeTypes getReturnType()
	{
		return NodeTypes.BOOLEAN;
	}

	@Override
	public boolean check()
	{
		// TODO Auto-generated method stub
		return false;
	}

}
