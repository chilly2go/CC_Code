package bran.data.structures.tree;

import java.util.ArrayList;

import bran.parser.SimpleNode;

public class ControlNodeIfCondition extends ControlNode
{

	public ControlNodeIfCondition(SimpleNode n)
	{
		super(n);
		this.restriction.setRange(1).setAllowedTypes(NodeTypes.BOOLEAN).addAllowedTypes(NodeTypes.COMPARE);
		this.type = NodeTypes.BOOLEAN;
	}

	@Override
	public boolean check()
	{
		if (this.restriction.contains(this.children.size()))
		{
			if (this.restriction.contains(this.children.get(0).getReturnType())) this.checked = true;
		}
		return this.checked;
	}

	@Override
	public NodeTypes getReturnType()
	{
		return null;
	}

	@Override
	public ArrayList<String> getCode(boolean lvalue) throws IllegalAccessException
	{
		// TODO Auto-generated method stub
		return null;
	}

}
