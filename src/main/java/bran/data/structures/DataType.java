package bran.data.structures;

public enum DataType {
	INT, BOOL, STRUCT, VOID
}
