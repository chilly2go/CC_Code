package bran.exceptions;

public class InvalidTreeStructureException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8242208087840698261L;

	public InvalidTreeStructureException()
	{
		super();
	}

	public InvalidTreeStructureException(String message)
	{
		super(message);
	}

	public InvalidTreeStructureException(Throwable cause)
	{
		super(cause);
	}

	public InvalidTreeStructureException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public InvalidTreeStructureException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
