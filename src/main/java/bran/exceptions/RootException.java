package bran.exceptions;

public class RootException extends IllegalStateException {

	private static final long serialVersionUID = -6826702718277664696L;

	public RootException(final String cause) {
		super(cause);
	}

	public RootException() {
		super();
	}
}
