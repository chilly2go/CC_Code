package bran.exceptions;

public class AllocSizeException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7608714995380478506L;

	public AllocSizeException()
	{
	}

	public AllocSizeException(String message)
	{
		super(message);
	}

	public AllocSizeException(Throwable cause)
	{
		super(cause);
	}

	public AllocSizeException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public AllocSizeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
